//! # Fitness calculation
//!
//! Fitness is the model representation of what the algorithm should strive for. Here,
//! we strive to model it as a single floating point value that should be maximized.

use std::marker::PhantomData;

use nalgebra::{DMatrix, Scalar};
use num_traits::{FromPrimitive, Num};

use crate::alias::{Fitness, Gene};
use crate::lineage::{Chromosome, Population};

/// Capability to evaluate the fitness values of chromosomes.
/// Usually, only `evaluate_genes` needs to be implemented.
pub trait Evaluator
where
    Self::Gene: Gene,
    Self::Fitness: Fitness,
{
    type Gene;
    type Fitness;
    /// Calculate the fitness of a slice of genes.
    fn evaluate_genes(&mut self, genes: &[Self::Gene]) -> Self::Fitness;
    /// Calculate or get the fitness of one chromosome.
    fn evaluate_one<'a>(
        &mut self,
        chromosome: &'a mut Chromosome<Self::Gene, Self::Fitness>,
    ) -> &'a Self::Fitness {
        if chromosome.fitness.as_ref().is_none() {
            chromosome.fitness = Some(self.evaluate_genes(&chromosome.genes))
        }
        chromosome.fitness.as_ref().unwrap()
    }
    /// Pre-process the input to set parameters on self.
    fn evaluate_pop_pre(
        &mut self,
        #[allow(unused_variables)] population: &Population<Self::Gene, Self::Fitness>,
    ) {
    }
    /// Evaluate the fitness of a complete population of chromosomes.
    fn evaluate_pop(&mut self, population: &mut Population<Self::Gene, Self::Fitness>) {
        self.evaluate_pop_pre(population);
        for chromosome in population.chromosomes.iter_mut() {
            self.evaluate_one(chromosome);
        }
    }
}

/// Fitness as the internally stored value.
#[derive(Clone, Debug, Default)]
pub struct EvaluatorValue<G, F> {
    pub value: F,
    _phantom: PhantomData<G>,
}
impl<G: Gene, F: Fitness> EvaluatorValue<G, F> {
    pub fn new(value: F) -> Self {
        Self {
            value,
            _phantom: PhantomData,
        }
    }
}
impl<G: Gene, F: Fitness> Evaluator for EvaluatorValue<G, F> {
    type Gene = G;
    type Fitness = F;
    fn evaluate_genes(&mut self, #[allow(unused_variables)] genes: &[Self::Gene]) -> F {
        self.value.clone()
    }
}

/// Fitness values from a closure.
#[derive(Clone, Debug, Default)]
pub struct EvaluatorClosure<G, F, C>
where
    C: FnMut(&[G]) -> F,
{
    pub closure: C,
    _phantom: PhantomData<G>,
}
impl<G, F, C> EvaluatorClosure<G, F, C>
where
    C: FnMut(&[G]) -> F,
{
    pub fn new(closure: C) -> Self {
        Self {
            closure,
            _phantom: PhantomData,
        }
    }
}
impl<G: Gene, F: Fitness, C> Evaluator for EvaluatorClosure<G, F, C>
where
    C: FnMut(&[G]) -> F,
{
    type Gene = G;
    type Fitness = F;
    fn evaluate_genes(&mut self, #[allow(unused_variables)] genes: &[Self::Gene]) -> F {
        (self.closure)(genes)
    }
}

/// Calculate the number of feedback marks given a gene sequence and a matrix problem.
///
/// # Arguments
///
/// * `genes` - The gene sequence to evaluate.
/// * `matrix` - The input matrix for the sequencing problem.
///
/// # Returns
///
/// The number of feedback marks.
pub fn feedback_marks<T: Num + Scalar>(genes: &[usize], matrix: &DMatrix<T>) -> usize {
    let mut result: usize = 0;
    let size = genes.len();
    // Skip the last row, can't have feedback
    for (idx, &row) in genes[..size - 1].iter().enumerate() {
        // Start from the first off diagonal cell.
        for &col in genes[(idx + 1)..].iter() {
            if !matrix[(row, col)].is_zero() {
                result += 1;
            }
        }
    }
    result
}

/// Fitness based on the minimum number of feedback marks.
///
/// Returns the number of non-feedback marks above the diagonal in the matrix (col > row).
pub fn fitness_feedback_marks<T: Num + Scalar>(genes: &[usize], matrix: &DMatrix<T>) -> usize {
    let size = matrix.nrows();
    let max_score = (size * (size - 1)) / 2; // empty upper matrix.
    max_score - feedback_marks(genes, matrix)
}

/// Fitness based on the minimum number of feedback marks.
///
/// See [`fitness_feedback_marks`] and [`feedback_marks`] for the inner workings.
#[derive(Clone, Debug)]
pub struct EvaluatorFeedbackMarks<T: Num + Scalar, F: Fitness> {
    pub matrix: DMatrix<T>,
    pub offset: usize,
    _phantom: PhantomData<F>,
}
impl<T: Num + Scalar, F: Fitness> EvaluatorFeedbackMarks<T, F> {
    pub fn new(matrix: DMatrix<T>, offset: usize) -> Self {
        Self {
            matrix,
            offset,
            _phantom: PhantomData,
        }
    }
}
impl<T: Num + Scalar, F: Fitness> Default for EvaluatorFeedbackMarks<T, F> {
    fn default() -> Self {
        Self {
            matrix: DMatrix::<T>::identity(0, 0),
            offset: usize::default(),
            _phantom: PhantomData,
        }
    }
}
impl<T: Num + Scalar, F: Fitness + FromPrimitive> Evaluator for EvaluatorFeedbackMarks<T, F> {
    type Gene = usize;
    type Fitness = F;
    fn evaluate_genes(&mut self, genes: &[Self::Gene]) -> Self::Fitness {
        FromPrimitive::from_usize(fitness_feedback_marks(genes, &self.matrix) + self.offset)
            .unwrap()
    }
}

/// Calculate the distance of feedback marks to the diagonal given a permutation
/// sequence for the rows and columns.
pub fn feedback_distance<T: Num + Scalar>(genes: &[usize], matrix: &DMatrix<T>) -> usize {
    let mut result: usize = 0;
    let size = genes.len();
    // Skip the last row, can't have feedback
    for (idx, &row) in genes[..size - 1].iter().enumerate() {
        // Start from the first off diagonal cell.
        for (dist, &col) in genes[(idx + 1)..].iter().enumerate() {
            if !matrix[(row, col)].is_zero() {
                result += dist + 1 // Pad with one to account for the skip above.
            }
        }
    }
    result
}

/// Fitness based on the minimum sum of the distance of feedback marks to the diagonal.
///
/// Returns the distance of non-feedback marks above the diagonal in the matrix (col > row).
pub fn fitness_feedback_distance<T: Num + Scalar>(genes: &[usize], matrix: &DMatrix<T>) -> usize {
    let size = matrix.nrows();
    let max_score = (size.pow(3) - size) / 6;
    max_score - feedback_distance(genes, matrix)
}

/// Fitness based on the minimum number of feedback distance.
///
/// See [`fitness_feedback_distance`] and [`feedback_distance`] for the inner workings.
#[derive(Clone, Debug)]
pub struct EvaluatorFeedbackDistance<T: Num + Scalar, F: Fitness> {
    pub matrix: DMatrix<T>,
    pub offset: usize,
    _phantom: PhantomData<F>,
}
impl<T: Num + Scalar, F: Fitness> EvaluatorFeedbackDistance<T, F> {
    pub fn new(matrix: DMatrix<T>, offset: usize) -> Self {
        Self {
            matrix,
            offset,
            _phantom: PhantomData,
        }
    }
}
impl<T: Num + Scalar, F: Fitness> Default for EvaluatorFeedbackDistance<T, F> {
    fn default() -> Self {
        Self {
            matrix: DMatrix::<T>::identity(0, 0),
            offset: usize::default(),
            _phantom: PhantomData,
        }
    }
}
impl<T: Num + Scalar, F: Fitness + FromPrimitive> Evaluator for EvaluatorFeedbackDistance<T, F> {
    type Gene = usize;
    type Fitness = F;
    fn evaluate_genes(&mut self, genes: &[Self::Gene]) -> Self::Fitness {
        FromPrimitive::from_usize(fitness_feedback_distance(genes, &self.matrix) + self.offset)
            .unwrap()
    }
}

/// Calculate the distance of marks to the lower left of the matrix.
pub fn lower_left_distance<T: Num + Scalar>(genes: &[usize], matrix: &DMatrix<T>) -> usize {
    let mut result: usize = 0;
    let size = genes.len();
    // Skip the last row, can't have feedback
    for (row_idx, &row) in genes.iter().enumerate() {
        // Start from the first off diagonal cell.
        for (col_idx, &col) in genes.iter().enumerate() {
            if !matrix[(row, col)].is_zero() {
                result += size + col_idx - row_idx - 1;
            }
        }
    }
    result
}

/// Fitness based on the minimum sum of the distance of marks to the lower left of the matrix.
///
/// Returns the distance of marks to the lower left at (size-1, 0).
pub fn fitness_lower_left_distance<T: Num + Scalar>(genes: &[usize], matrix: &DMatrix<T>) -> usize {
    let size = matrix.nrows();
    let max_score = (size - 1) * size * size;
    max_score - lower_left_distance(genes, matrix)
}

/// Fitness based on the minimum number of feedback distance.
///
/// See [`fitness_feedback_distance`] and [`feedback_distance`] for the inner workings.
#[derive(Clone, Debug)]
pub struct EvaluatorLowerLeftDistance<T: Num + Scalar, F: Fitness> {
    pub matrix: DMatrix<T>,
    pub offset: usize,
    _phantom: PhantomData<F>,
}
impl<T: Num + Scalar, F: Fitness> EvaluatorLowerLeftDistance<T, F> {
    pub fn new(matrix: DMatrix<T>, offset: usize) -> Self {
        Self {
            matrix,
            offset,
            _phantom: PhantomData,
        }
    }
}
impl<T: Num + Scalar, F: Fitness> Default for EvaluatorLowerLeftDistance<T, F> {
    fn default() -> Self {
        Self {
            matrix: DMatrix::<T>::identity(0, 0),
            offset: usize::default(),
            _phantom: PhantomData,
        }
    }
}
impl<T: Num + Scalar, F: Fitness + FromPrimitive> Evaluator for EvaluatorLowerLeftDistance<T, F> {
    type Gene = usize;
    type Fitness = F;
    fn evaluate_genes(&mut self, genes: &[Self::Gene]) -> Self::Fitness {
        FromPrimitive::from_usize(lower_left_distance(genes, &self.matrix) + self.offset).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use nalgebra::dmatrix;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    use rand::prelude::*;
    use rand_chacha::ChaCha8Rng;

    #[allow(unused_imports)]
    use super::*;
    use crate::generator::{Generator, GeneratorRandomSequence};
    fn get_fixed_rng() -> ChaCha8Rng {
        ChaCha8Rng::seed_from_u64(0)
    }

    #[test]
    fn smoke_fitness() {
        let mut rng = get_fixed_rng();

        let matrix: DMatrix<f64> = dmatrix![0.0, 5.0;
        0.0, 0.0];
        let mut evaluator = EvaluatorFeedbackMarks::new(matrix, 0);

        assert_eq!(
            evaluator.evaluate_genes(&[0, 1]),
            0,
            "Should be 1.0 fitness (the worst)."
        );
        assert_eq!(
            evaluator.evaluate_genes(&[1, 0]),
            1,
            "Should be 1.0 fitness (the best)."
        );

        // Test the population method, too.
        let mut generator = GeneratorRandomSequence::<usize, _>::default();
        let mut population: Population<usize, usize> = generator.generate_pop(&mut rng, 2, 5);
        evaluator.evaluate_pop(&mut population);
        let fitnesses: Vec<usize> = population.fitness().copied().collect();
        assert_eq!(&fitnesses, &vec![0, 1, 1, 0, 1]);
    }
}
