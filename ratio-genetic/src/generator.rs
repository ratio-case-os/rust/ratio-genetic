//! # Chromosomes generators
//!
//! Generation of chromosomes is usually the beginning of a genetic algorithm, but can
//! also be utilized to "fill in the gaps" in certain evolutionary strategies where a
//! certain percentage of the offspring is considered to be introduced externally, being
//! not via selection or crossover.

use std::marker::PhantomData;

use num_traits::FromPrimitive;
use rand::prelude::*;

use crate::alias::{Fitness, Gene};
use crate::lineage::{Chromosome, Population};

/// Capability to generate new genes, chromosomes, and populations.
pub trait Generator<Rnd: Rng + ?Sized>
where
    Self::Gene: Gene,
    Self::Fitness: Fitness,
{
    type Gene;
    type Fitness;

    /// Generate a gene sequence of a given length.
    fn generate_genes(&mut self, rng: &mut Rnd, n_genes: usize) -> Vec<Self::Gene>;
    /// Generate a single chromosome with a given number of genes.
    fn generate_one(
        &mut self,
        rng: &mut Rnd,
        n_genes: usize,
    ) -> Chromosome<Self::Gene, Self::Fitness> {
        Chromosome::new(self.generate_genes(rng, n_genes))
    }
    /// Pre-process the input to set parameters on self.
    fn generate_pop_pre(
        &mut self,
        #[allow(unused_variables)] n_genes: usize,
        #[allow(unused_variables)] n_chromosomes: usize,
    ) {
    }
    /// Generate a population of given number of chromosomes and a given number of genes per chromosome.
    fn generate_pop(
        &mut self,
        rng: &mut Rnd,
        n_genes: usize,
        n_chromosomes: usize,
    ) -> Population<Self::Gene, Self::Fitness> {
        self.generate_pop_pre(n_genes, n_chromosomes);
        let mut chromosomes = vec![];
        chromosomes.resize_with(n_chromosomes, || self.generate_one(rng, n_genes));
        Population::new(chromosomes, None)
    }
}

/// Generate a random gene sequence containing all numbers of 0..N-1 in random order.
pub fn generate_random_sequence<Rnd: Rng + ?Sized, G: Gene + FromPrimitive>(
    rng: &mut Rnd,
    n_genes: usize,
) -> Vec<G> {
    rand::seq::index::sample(rng, n_genes, n_genes)
        .iter()
        .map(|x| G::from_usize(x).unwrap())
        .collect()
}

/// Generator that generates chromosomes with randomized gene sequences.
#[derive(Clone, Debug, Default)]
pub struct GeneratorRandomSequence<G: Gene, F: Fitness> {
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness> GeneratorRandomSequence<G, F> {
    pub fn new() -> Self {
        Self::default()
    }
}
impl<Rnd: Rng + ?Sized, G: Gene + FromPrimitive, F: Fitness> Generator<Rnd>
    for GeneratorRandomSequence<G, F>
{
    type Gene = G;
    type Fitness = F;

    fn generate_genes(&mut self, rng: &mut Rnd, n_genes: usize) -> Vec<Self::Gene> {
        generate_random_sequence(rng, n_genes)
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    use rand_chacha::ChaCha8Rng;

    #[allow(unused_imports)]
    use super::*;
    fn get_fixed_rng() -> ChaCha8Rng {
        ChaCha8Rng::seed_from_u64(0)
    }

    #[test]
    fn generate_chromosome() {
        let mut rng: ChaCha8Rng = get_fixed_rng();
        let mut generator = GeneratorRandomSequence::<usize, f64>::new();
        let chromosome = generator.generate_one(&mut rng, 248);
        assert_eq!(chromosome.len(), 248);
        let reference: HashSet<usize> = (0..248).collect();
        let values: HashSet<usize> = chromosome.genes.iter().cloned().collect();
        assert_eq!(values, reference);
    }

    #[test]
    fn generate_population() {
        let mut rng = get_fixed_rng();
        let mut generator = GeneratorRandomSequence::<usize, f64>::new();
        let pop: Population<usize, f64> = generator.generate_pop(&mut rng, 3, 6);
        let reference: Vec<Chromosome<usize, f64>> = [
            vec![0, 1, 2],
            vec![1, 0, 2],
            vec![2, 1, 0],
            vec![0, 2, 1],
            vec![2, 0, 1],
            vec![0, 2, 1], // Notice the dupe.
        ]
        .iter()
        .map(|genes: &Vec<usize>| Chromosome::new(genes.to_owned()))
        .collect();
        assert_eq!(pop.chromosomes, reference);
    }
}
