//! # Convergence procedures
//!
//! Convergence allows for calculating a given metric or set of metrics that indicate
//! whether the genetic algorithm's is considered converged and may be allowed to exit
//! early. That is, it exits before the maximum number of generations has been cycled
//! through. Common indicators are a lack of change in the hall of fame, or a reduced
//! variance in the generation's chromosomes or fitness values.

use std::marker::PhantomData;

use crate::alias::{Fitness, Gene, Record};
use crate::lineage::Lineage;

/// Capability to calculate whether the algorithm has converged and can exit early.
///
/// By default it's set to never converge and play out the set number of generations.
pub trait Convergence
where
    Self::Gene: Gene,
    Self::Fitness: Fitness,
    Self::Record: Record,
{
    type Gene;
    type Fitness;
    type Record;
    /// Whether the genetic algorithm's solution is considered converged and can exit early.
    fn is_converged(&mut self, lineage: &Lineage<Self::Gene, Self::Fitness, Self::Record>) -> bool;
}

/// Never converge.
#[derive(Clone, Debug, Default)]
pub struct ConvergenceNever<G, F, R> {
    _phantom: PhantomData<(G, F, R)>,
}
impl<G, F, R> ConvergenceNever<G, F, R>
where
    G: Gene,
    F: Fitness,
    R: Record,
{
    pub fn new() -> Self {
        Self::default()
    }
}
impl<G, F, R> Convergence for ConvergenceNever<G, F, R>
where
    G: Gene,
    F: Fitness,
    R: Record,
{
    type Gene = G;
    type Fitness = F;
    type Record = R;
    fn is_converged(
        &mut self,
        #[allow(unused_variables)] lineage: &Lineage<Self::Gene, Self::Fitness, Self::Record>,
    ) -> bool {
        false
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};

    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn smoke_convergence() {
        let mut convergence = ConvergenceNever::new();
        assert!(!convergence.is_converged(&Lineage::<usize, usize, usize>::default()));
    }
}
