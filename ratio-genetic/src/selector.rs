//! # Selection procedures
//!
//! Selection is a stage in genetic algorithms that usually assigns fitter chromosomes a
//! higher probability to make it to subsequent generations. By culling lesser
//! performant chromosomes or promoting the fitter chromosomes, the variety in the
//! offspring is somewhat reduced. Other evolutionary stages such as crossover
//! reintroduce this variety.

use std::marker::PhantomData;

use num_traits::AsPrimitive;
use rand::prelude::*;

use crate::alias::{Fitness, Gene};
use crate::lineage::{Chromosome, Population};

/// Capability to select a chromosomes out of a population.
pub trait Selector<R: Rng + ?Sized>
where
    Self::Gene: Gene,
    Self::Fitness: Fitness,
{
    type Gene;
    type Fitness;

    /// Selection method for one chromosome from a population.
    fn select_one<'a>(
        &mut self,
        rng: &mut R,
        population: &'a Population<Self::Gene, Self::Fitness>,
    ) -> &'a Chromosome<Self::Gene, Self::Fitness> {
        select_one_random(rng, population)
    }
    /// Process input population to set some parameters. Does nothing by default.
    fn select_pop_pre(
        &mut self,
        #[allow(unused_variables)] population: &Population<Self::Gene, Self::Fitness>,
        #[allow(unused_variables)] n_chromosomes: Option<usize>,
    ) {
    }
    /// Repeatable selection method for generating a selected offspring.
    ///
    /// # Arguments
    ///
    /// * `rng` - Random number generator.
    /// * `population` - Population to select chromosomes from.
    /// * `n_chromosomes` -  Desired population size. If `None`, keep size identical to the input population.
    ///
    /// # Returns
    ///
    /// A new population with selected chromosomes (cloned). Can contain duplicates.
    fn select_pop(
        &mut self,
        rng: &mut R,
        population: &Population<Self::Gene, Self::Fitness>,
        n_chromosomes: Option<usize>,
    ) -> Population<Self::Gene, Self::Fitness> {
        self.select_pop_pre(population, n_chromosomes);
        let size = n_chromosomes.unwrap_or(population.len());
        let mut offspring = Population::default();
        for _ in 0..size {
            offspring.add_chromosome(self.select_one(rng, population).to_owned());
        }
        offspring
    }
}

/// Select a random chromosome without any fitness preference.
pub fn select_one_random<'a, Rnd: Rng + ?Sized, G: Gene, F: Fitness>(
    rng: &mut Rnd,
    population: &'a Population<G, F>,
) -> &'a Chromosome<G, F> {
    population.get_chromosome(rng.gen_range(0..population.len()))
}
/// Selector that selects chromosomes randomly.
#[derive(Clone, Debug, Default)]
pub struct SelectorRandom<G: Gene, F: Fitness> {
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness> SelectorRandom<G, F> {
    pub fn new() -> Self {
        Self::default()
    }
}
impl<Rnd: Rng + ?Sized, G: Gene, F: Fitness> Selector<Rnd> for SelectorRandom<G, F> {
    type Gene = G;
    type Fitness = F;
    fn select_one<'a>(
        &mut self,
        rng: &mut Rnd,
        population: &'a Population<Self::Gene, Self::Fitness>,
    ) -> &'a Chromosome<Self::Gene, Self::Fitness> {
        select_one_random(rng, population)
    }
}

/// Utility method to get the cumulative sum vector of a float iterator.
pub fn cumulative_sum(iterator: impl Iterator<Item = f64>) -> Vec<f64> {
    iterator
        .scan(0.0, |acc, fitness| {
            *acc += fitness;
            Some(*acc)
        })
        .collect()
}

/// Selection method for one chromosome where each chromosome is assigned a piece of a
/// roulette wheel with a size equal to it's relative fitness. An individual with higher
/// fitness is assigned a larger piece of a roulette wheel.
///
/// It is recommended that the fitness values are all >0.0, as this could result in
/// unpredictable behavior and this method is meant to assign a probability to any
/// chromosome without exceptions.
pub fn select_one_roulette<'a, Rnd: Rng + ?Sized, G: Gene, F: Fitness>(
    rng: &mut Rnd,
    population: &'a Population<G, F>,
    fitness_cum: &[f64],
) -> &'a Chromosome<G, F> {
    let threshold: f64 = fitness_cum.last().unwrap() * rng.gen::<f64>();
    for (idx, &fitness) in fitness_cum.iter().enumerate() {
        if fitness >= threshold {
            return population.get_chromosome(idx);
        }
    }
    population.get_chromosome(population.len() - 1)
}
/// Selection method according to relative fitness. See [`select_one_roulette`] for the
/// inner workings.
#[derive(Clone, Debug, Default)]
pub struct SelectorRoulette<G: Gene, F: Fitness> {
    fitness_cum: Vec<f64>,
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness + AsPrimitive<f64>> SelectorRoulette<G, F> {
    /// Create a new instance. See [`select_one_roulette`] for the inner workings.
    pub fn new() -> Self {
        Self::default()
    }
}
impl<Rnd: Rng + ?Sized, G: Gene, F: Fitness + AsPrimitive<f64>> Selector<Rnd>
    for SelectorRoulette<G, F>
{
    type Gene = G;
    type Fitness = F;

    fn select_one<'a>(
        &mut self,
        rng: &mut Rnd,
        population: &'a Population<Self::Gene, Self::Fitness>,
    ) -> &'a Chromosome<Self::Gene, Self::Fitness> {
        select_one_roulette(rng, population, &self.fitness_cum)
    }
    fn select_pop_pre(
        &mut self,
        population: &Population<Self::Gene, Self::Fitness>,
        #[allow(unused_variables)] n_chromosomes: Option<usize>,
    ) {
        self.fitness_cum = cumulative_sum(population.fitness().map(|x| x.as_()))
    }
}

/// Tournament selection for chromosomes.
///
/// Modifies the population in place, by randomly removing `n_tournament` chromosomes
/// and returning only the best one.
pub fn select_one_tournament<'a, Rnd: Rng + ?Sized, G: Gene, F: Fitness>(
    rng: &mut Rnd,
    population: &'a Population<G, F>,
    n_competitors: usize,
) -> &'a Chromosome<G, F> {
    let candidate_indices: Vec<usize> =
        rand::seq::index::sample(rng, population.len(), n_competitors).into_vec();
    let mut pick = population.get_chromosome(candidate_indices[0]);
    for i in candidate_indices {
        let candidate = population.get_chromosome(i);
        if candidate.fitness > pick.fitness {
            pick = candidate;
        }
    }
    pick
}

/// Selector that selects chromosomes based on tournaments of N competing chromosomes.
///
/// It is up to the user to ensure that the number of competitors is compatible with the
/// population's size.
#[derive(Clone, Debug, Default)]
pub struct SelectorTournament<G: Gene, F: Fitness> {
    n_competitors: usize,
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness> SelectorTournament<G, F> {
    pub fn new(n_competitors: usize) -> Self {
        Self {
            n_competitors,
            _phantom: PhantomData,
        }
    }
}
impl<Rnd: Rng + ?Sized, G: Gene, F: Fitness> Selector<Rnd> for SelectorTournament<G, F> {
    type Gene = G;
    type Fitness = F;
    fn select_one<'a>(
        &mut self,
        rng: &mut Rnd,
        population: &'a Population<Self::Gene, Self::Fitness>,
    ) -> &'a Chromosome<Self::Gene, Self::Fitness> {
        select_one_tournament(rng, population, self.n_competitors)
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    use rand_chacha::ChaCha8Rng;

    #[allow(unused_imports)]
    use super::*;
    use crate::generator::{Generator, GeneratorRandomSequence};
    fn get_fixed_rng() -> ChaCha8Rng {
        ChaCha8Rng::seed_from_u64(0)
    }

    #[test]
    fn smoke_select_default() {
        let mut rng = get_fixed_rng();
        let mut generator = GeneratorRandomSequence::<usize, _>::new();
        let mut selector = SelectorRandom::new();
        let population: Population<usize, f64> = generator.generate_pop(&mut rng, 3, 10);
        let offspring = selector.select_pop(&mut rng, &population, Some(1));
        assert_eq!(&offspring.len(), &1);
        assert_eq!(offspring.get_chromosome(0).genes, &[2, 0, 1]);
    }

    #[test]
    fn smoke_select_one_roulette() {
        let mut rng = get_fixed_rng();

        let mut generator = GeneratorRandomSequence::<usize, _>::new();
        let mut selector = SelectorRoulette::new();
        let mut population = generator.generate_pop(&mut rng, 4, 20);
        for (idx, chromo) in population.chromosomes.iter_mut().enumerate() {
            chromo.fitness = Some(idx as f64);
        }
        let fit_sum: f64 = population.fitness().sum();
        assert_eq!(fit_sum, (0..population.len()).map(|x| x as f64).sum());

        let chromo = select_one_roulette(
            &mut rng,
            &population,
            &cumulative_sum(population.fitness().copied()),
        );
        assert_eq!(
            chromo,
            population.get_chromosome(13),
            "Randomness, but fixed rng."
        );

        let chromo = select_one_roulette(
            &mut rng,
            &population,
            &cumulative_sum(population.fitness().copied()),
        );
        assert_eq!(
            chromo,
            population.get_chromosome(12),
            "Randomness, but fixed rng."
        );

        let offspring = selector.select_pop(&mut rng, &population, None);
        let pop_sum: f64 = population.fitness().sum();
        let off_sum: f64 = offspring.fitness().sum();
        assert!(
            off_sum > pop_sum,
            "Fitness should have improved. Usually ;)"
        );

        assert_eq!(
            population.len(),
            offspring.len(),
            "Population length should remain equal."
        );

        let pop_set: HashSet<Vec<usize>> =
            HashSet::from_iter(population.chromosomes.iter().map(|x| x.genes.clone()));
        let off_set: HashSet<Vec<usize>> =
            HashSet::from_iter(offspring.chromosomes.iter().map(|x| x.genes.clone()));
        assert!(
            off_set.len() < pop_set.len(),
            "Number of unique chromosomes will have reduced."
        );

        let offspring = selector.select_pop(&mut rng, &population, Some(8));
        assert_eq!(offspring.len(), 8);
    }
}
