//! # Recorder functionality
//!
//! Record characteristics or statistical values for a population.

use std::marker::PhantomData;

use hdrhistogram::Histogram;
use num_traits::AsPrimitive;

use crate::alias::{Fitness, Gene, Record};
use crate::lineage::Population;

/// Capability to calculate statistics from a population.
pub trait Recorder
where
    Self::Gene: Gene,
    Self::Fitness: Fitness,
    Self::Record: Record,
{
    type Gene;
    type Fitness;
    type Record;
    /// Calculate the record's contents.
    fn record_pop(&mut self, population: &Population<Self::Gene, Self::Fitness>) -> Self::Record;
}

/// Fitness statistics record.
#[derive(Clone, Debug, Default)]
pub struct FitnessStatistics {
    /// Size of the population.
    pub size: usize,
    /// Minimum fitness value.
    pub min: f64,
    /// Maximum fitness value.
    pub max: f64,
    /// Fitness sum.
    pub sum: f64,
    /// Fitness mean.
    pub mean: f64,
    /// Fitness variance.
    pub variance: f64,
}

/// Record basic numerical fitness statistics of a population's chromosomes.
pub fn record_fitness_statistics<G: Gene, F: Fitness + AsPrimitive<f64>>(
    population: &Population<G, F>,
) -> FitnessStatistics {
    let size = population.len();
    let fitness: Vec<f64> = population.fitness().map(|x| x.as_()).collect();
    let sum = fitness.iter().copied().sum();
    let mut mean = 0.0;
    let mut variance = 0.0;
    let mut min = 0.0;
    let mut max = 0.0;
    if size > 0 {
        mean = match size {
            0 => 0.0,
            _ => sum / (size as f64),
        };
        let mut fitnesses = fitness.iter();
        let first = fitnesses.next().unwrap();
        min = *first;
        max = *first;
        for &value in fitnesses {
            let diff = mean - value;
            variance += diff * diff;
            if value < min {
                min = value
            }
            if value > max {
                max = value
            }
        }
    }
    FitnessStatistics {
        size,
        min,
        max,
        sum,
        mean,
        variance,
    }
}
/// Recorder that captures basic numerical statistics regarding fitness.
#[derive(Clone, Debug, Default)]
pub struct RecorderFitnessStatistics<G, F> {
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness> RecorderFitnessStatistics<G, F> {
    pub fn new() -> Self {
        Self::default()
    }
}
impl<G: Gene, F: Fitness + AsPrimitive<f64>> Recorder for RecorderFitnessStatistics<G, F> {
    type Gene = G;
    type Fitness = F;
    type Record = FitnessStatistics;
    fn record_pop(&mut self, population: &Population<Self::Gene, Self::Fitness>) -> Self::Record {
        record_fitness_statistics(population)
    }
}

/// Wrap the Histogram into a crate-managed struct so we can implement traits.
pub struct HdrHistogram(Histogram<u64>);
impl HdrHistogram {
    pub fn new(sigfig: u8) -> Self {
        Self(Histogram::new(sigfig).unwrap())
    }
}
impl Default for HdrHistogram {
    fn default() -> Self {
        Self(Histogram::new(3).unwrap())
    }
}

/// Create a wrapped [`Histogram`] as a means of a population's fitness record.
pub fn record_hdrhistogram<G: Gene, F: Fitness + AsPrimitive<u64>>(
    population: &Population<G, F>,
    sigfig: u8,
) -> HdrHistogram {
    let mut hist = HdrHistogram::new(sigfig);
    for f in population.fitness() {
        hist.0.record(f.as_()).unwrap();
    }
    hist
}

/// Recorder that calculates a HdrHistogram for the fitness.
#[derive(Clone, Debug, Default)]
pub struct RecorderHdrHistogram<G, F> {
    /// Significant figures that are kept by the HdrHistogram.
    pub sigfig: u8,
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness + AsPrimitive<u64>> RecorderHdrHistogram<G, F> {
    /// Create a new HdrHistogram recorder instance with the given number of significant figures.
    pub fn new(sigfig: u8) -> Self {
        Self {
            sigfig,
            _phantom: PhantomData,
        }
    }
}
impl<G: Gene, F: Fitness + AsPrimitive<u64>> Recorder for RecorderHdrHistogram<G, F> {
    type Gene = G;
    type Fitness = F;
    type Record = HdrHistogram;
    fn record_pop(&mut self, population: &Population<Self::Gene, Self::Fitness>) -> Self::Record {
        record_hdrhistogram(population, self.sigfig)
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    use rand::prelude::*;
    use rand_chacha::ChaCha8Rng;

    #[allow(unused_imports)]
    use super::*;
    use crate::evaluator::{Evaluator, EvaluatorClosure};
    use crate::generator::{Generator, GeneratorRandomSequence};
    fn get_fixed_rng() -> ChaCha8Rng {
        ChaCha8Rng::seed_from_u64(0)
    }

    #[test]
    fn smoke_recorder() {
        let mut rng = get_fixed_rng();
        let mut recorder = RecorderFitnessStatistics::default();
        let mut generator = GeneratorRandomSequence::<usize, f64>::new();
        let mut population = generator.generate_pop(&mut rng, 10, 100);
        let closure = |_: &[usize]| 2.0;
        let mut evaluator = EvaluatorClosure::<usize, f64, _>::new(closure);
        evaluator.evaluate_pop(&mut population);

        let record = recorder.record_pop(&population);

        assert_eq!(record.size, 100, "Size record value");
        assert_eq!(record.min, 2.0, "Minimum record value");
        assert_eq!(record.max, 2.0, "Maximum record value");
        assert_eq!(record.mean, 2.0, "Mean record value");
        assert_eq!(record.sum, 200.0, "Sum record value");
        assert_eq!(record.variance, 0.0, "Variance record value");
    }

    #[test]
    fn smoke_hdrhistogram() {
        let mut rng = get_fixed_rng();
        let mut recorder = RecorderHdrHistogram::default();
        let mut generator = GeneratorRandomSequence::<usize, _>::new();
        let mut population = generator.generate_pop(&mut rng, 10, 100);
        let closure = |_: &[usize]| rng.gen_range::<usize, _>(0..5);
        let mut evaluator = EvaluatorClosure::<usize, usize, _>::new(closure);
        evaluator.evaluate_pop(&mut population);
        let record = recorder.record_pop(&population);
        assert_eq!(record.0.len(), 100);
        assert_eq!(record.0.count_at(2), 39);
    }
}
