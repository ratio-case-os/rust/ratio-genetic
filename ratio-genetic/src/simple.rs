//! # Simple Genetic Algorithm (SGA) procedures
//!

use rand::Rng;

use crate::alias::{Fitness, Gene, Record};
use crate::convergence::Convergence;
use crate::crossover::Crossover;
use crate::evaluator::Evaluator;
use crate::generator::Generator;
use crate::lineage::{Lineage, Population};
use crate::mutator::Mutator;
use crate::recorder::Recorder;
use crate::selector::Selector;

/// Common settings for a simple genetic algorithm.
#[derive(Clone, Debug, Default)]
pub struct SimpleGeneticSettings {
    /// Number of genes in each chromosome to generate if needed.
    pub n_genes: Option<usize>,
    /// Number of chromosomes to strive for in each population. (should be even)
    pub n_chromosomes: Option<usize>,
    /// Maximum number of generations to evaluate.
    pub n_generations: Option<usize>,
    /// Maximum number of generation records to keep.
    pub n_records: Option<usize>,
    /// Number of best performing chromosomes to keep.
    pub n_hall_of_fame: Option<usize>,
    /// Probability of crossover happening between each pair of chromosomes.
    pub p_crossover: f64,
    /// Probability of mutation happening for each chromosome.
    pub p_mutation: f64,
}
impl SimpleGeneticSettings {
    pub fn new(
        n_genes: Option<usize>,
        n_chromosomes: Option<usize>,
        n_generations: Option<usize>,
        n_records: Option<usize>,
        n_hall_of_fame: Option<usize>,
        p_crossover: f64,
        p_mutation: f64,
    ) -> Self {
        Self {
            n_genes,
            n_chromosomes,
            n_generations,
            n_records,
            n_hall_of_fame,
            p_crossover,
            p_mutation,
        }
    }
}

/// Common operators for a simple genetic algorithm.
pub struct SimpleGeneticOperators<'a, Rnd: Rng + ?Sized, G: Gene, F: Fitness, R: Record> {
    pub generator: &'a mut dyn Generator<Rnd, Gene = G, Fitness = F>,
    pub evaluator: &'a mut dyn Evaluator<Gene = G, Fitness = F>,
    pub recorder: &'a mut dyn Recorder<Gene = G, Fitness = F, Record = R>,
    pub selector: &'a mut dyn Selector<Rnd, Gene = G, Fitness = F>,
    pub crossover: &'a mut dyn Crossover<Rnd, Gene = G, Fitness = F>,
    pub mutator: &'a mut dyn Mutator<Rnd, Gene = G, Fitness = F>,
    pub convergence: &'a mut dyn Convergence<Gene = G, Fitness = F, Record = R>,
}
impl<'a, Rnd: Rng + ?Sized, G: Gene, F: Fitness, R: Record>
    SimpleGeneticOperators<'a, Rnd, G, F, R>
{
    pub fn new(
        generator: &'a mut dyn Generator<Rnd, Gene = G, Fitness = F>,
        evaluator: &'a mut dyn Evaluator<Gene = G, Fitness = F>,
        recorder: &'a mut dyn Recorder<Gene = G, Fitness = F, Record = R>,
        selector: &'a mut dyn Selector<Rnd, Gene = G, Fitness = F>,
        crossover: &'a mut dyn Crossover<Rnd, Gene = G, Fitness = F>,
        mutator: &'a mut dyn Mutator<Rnd, Gene = G, Fitness = F>,
        convergence: &'a mut dyn Convergence<Gene = G, Fitness = F, Record = R>,
    ) -> Self {
        Self {
            generator,
            evaluator,
            recorder,
            selector,
            crossover,
            mutator,
            convergence,
        }
    }
}

/// A Simple Genetic Algorithm (SGA) consisting of:
///
/// 1. A generation step, initializing the population, whose fitness is evaluated.
/// 2. An evolution step, that adds a new offspring, whose fitness is also evaluated.
/// 3. Convergence check, that allows for early exiting.
pub trait SimpleGeneticAlgorithm
where
    Self::Gene: Gene,
    Self::Fitness: Fitness,
    Self::Record: Default,
{
    type Gene;
    type Fitness;
    type Record;

    /// Evolve a population into a new offspring. Assumes fitness is evaluated.
    fn evolve<Rnd: Rng + ?Sized>(
        &self,
        rng: &mut Rnd,
        population: &Population<Self::Gene, Self::Fitness>,
        settings: &SimpleGeneticSettings,
        operators: &mut SimpleGeneticOperators<'_, Rnd, Self::Gene, Self::Fitness, Self::Record>,
    ) -> Population<Self::Gene, Self::Fitness> {
        // Next population index.
        let next = population.generation + 1;
        // Full selection.
        let population = operators
            .selector
            .select_pop(rng, population, settings.n_chromosomes);
        // Full crossover. That is, each pair has a chance of p_crossover.
        let population = operators.crossover.crossover_pop(
            rng,
            &population,
            settings.n_chromosomes,
            settings.p_crossover,
        );
        // Full mutation. That is, each chromosome has a chance to mutate.
        let mut population = operators.mutator.mutate_pop(
            rng,
            &population,
            settings.n_chromosomes,
            settings.p_mutation,
        );
        // Set index and return.
        population.generation = next;
        population
    }

    /// Execute the Simple Genetic Algorithm.
    ///
    /// # Arguments:
    ///
    /// * `rng` - Random number generator.
    /// * `lineage` - Optional input lineage to build further upon.
    /// * `settings` - Common simple genetic algorithm settings.
    /// * `operators` - Simple genetic algorithm operators.
    ///
    /// # Returns
    ///
    /// Resulting lineage after the set number of generations or when having achieved
    /// solution convergence.
    fn execute<R: Rng + ?Sized>(
        &self,
        rng: &mut R,
        lineage: Option<Lineage<Self::Gene, Self::Fitness, Self::Record>>,
        settings: &SimpleGeneticSettings,
        operators: &mut SimpleGeneticOperators<'_, R, Self::Gene, Self::Fitness, Self::Record>,
    ) -> Lineage<Self::Gene, Self::Fitness, Self::Record> {
        let mut lineage = match lineage {
            Some(lineage) => lineage,
            None => {
                let population = operators.generator.generate_pop(
                    rng,
                    settings.n_genes.unwrap(),
                    settings.n_chromosomes.unwrap(),
                );
                Lineage::from_population(
                    population,
                    operators.evaluator,
                    operators.recorder,
                    settings.n_generations,
                    settings.n_records,
                    settings.n_hall_of_fame,
                )
            }
        };

        while !operators.convergence.is_converged(&lineage)
            && (settings.n_generations.is_none()
                || lineage.current().generation < settings.n_generations.unwrap())
        {
            lineage.add_generation(
                self.evolve(rng, lineage.current(), settings, operators),
                operators.evaluator,
                operators.recorder,
            );
        }
        lineage
    }
}

#[cfg(test)]
mod tests {
    use nalgebra::dmatrix;
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    use rand::prelude::*;
    use rand_chacha::ChaCha8Rng;

    #[allow(unused_imports)]
    use super::*;
    use crate::convergence::ConvergenceNever;
    use crate::crossover::CrossoverIPX;
    use crate::evaluator::EvaluatorFeedbackMarks;
    use crate::generator::GeneratorRandomSequence;
    use crate::mutator::MutatorSwap;
    use crate::recorder::{FitnessStatistics, RecorderFitnessStatistics};
    use crate::selector::SelectorRoulette;
    fn get_fixed_rng() -> ChaCha8Rng {
        ChaCha8Rng::seed_from_u64(0)
    }

    impl SimpleGeneticAlgorithm for SimpleGeneticSettings {
        type Gene = usize;
        type Fitness = f64;
        type Record = FitnessStatistics;
    }

    #[test]
    fn smoke_sequencing() {
        let mut rng = get_fixed_rng();
        let matrix = dmatrix![
            0.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0;
            0.0,0.0,1.0,1.0,1.0,1.0,1.0,1.0;
            0.0,0.0,0.0,1.0,1.0,1.0,1.0,1.0;
            0.0,0.0,0.0,0.0,1.0,1.0,1.0,1.0;
            0.0,0.0,0.0,0.0,0.0,1.0,1.0,1.0;
            0.0,0.0,0.0,0.0,0.0,0.0,1.0,1.0;
            0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0;
            0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0
        ];
        let n_genes = matrix.shape().0;

        let mut generator = GeneratorRandomSequence::<usize, _>::new();
        let mut evaluator = EvaluatorFeedbackMarks::new(matrix, 1);
        let mut selector = SelectorRoulette::new();
        let mut crossover = CrossoverIPX::new();
        let mut mutator = MutatorSwap::new(0.05);
        let mut convergence = ConvergenceNever::new();
        let mut recorder = RecorderFitnessStatistics::new();

        let sga = SimpleGeneticSettings::new(
            Some(n_genes),
            Some(1000),
            Some(300),
            Some(1),
            Some(1),
            0.03,
            0.01,
        );
        let mut operators = SimpleGeneticOperators::new(
            &mut generator,
            &mut evaluator,
            &mut recorder,
            &mut selector,
            &mut crossover,
            &mut mutator,
            &mut convergence,
        );
        let lin = sga.execute(&mut rng, None, &sga, &mut operators);
        let max_score = 0.5 * (n_genes as f64 * (n_genes as f64 - 1.0)) + 1.0;
        let best = lin.hall_of_fame.chromosomes.first().unwrap();
        let rev: Vec<usize> = (0..n_genes).rev().collect();
        assert_eq!(&best.genes, &rev);
        assert_eq!(
            lin.hall_of_fame
                .chromosomes
                .first()
                .unwrap()
                .fitness
                .unwrap(),
            max_score
        );
    }
}
