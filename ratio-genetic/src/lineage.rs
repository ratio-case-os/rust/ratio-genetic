//! # Lineage storage
//!
//! The lineage in a genetic algorithm is where the data resides. It keeps track of past
//! populations of chromosomes (generations) in an efficient way and also of the most
//! performant chromosomes in all past populations in the hall of fame.

use std::collections::{HashSet, VecDeque};

use crate::alias::{Fitness, Gene, Record};
use crate::evaluator::Evaluator;
use crate::recorder::Recorder;

/// A chromosome represented by a vector of genes and a fitness value.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Chromosome<G: Gene, F: Fitness> {
    /// The genes of this chromosome.
    pub genes: Vec<G>,
    /// The fitness of this chromosome.
    pub fitness: Option<F>,
}
impl<G: Gene, F: Fitness> Chromosome<G, F> {
    /// Create a new chromosome.
    pub fn new(genes: Vec<G>) -> Self {
        Chromosome {
            genes,
            fitness: None,
        }
    }
    /// Create a new chromosome from a slice of genes.
    pub fn from_slice(genes: &[G]) -> Self {
        Chromosome {
            genes: Vec::from(genes),
            fitness: None,
        }
    }
    /// Contained gene sequence slice representation.
    pub fn as_slice(&self) -> &[G] {
        self.genes.as_slice()
    }
    /// Contained gene sequence length.
    pub fn len(&self) -> usize {
        self.genes.len()
    }
    /// Whether this instance's gene sequence is empty.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

/// A population consisting of multiple chromosomes.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Population<G: Gene, F: Fitness> {
    /// Chromosomes of this population.
    pub chromosomes: Vec<Chromosome<G, F>>,
    /// Generation index.
    pub generation: usize,
}
impl<G: Gene, F: Fitness> Population<G, F> {
    /// Create a new population.
    pub fn new(chromosomes: Vec<Chromosome<G, F>>, generation: Option<usize>) -> Self {
        Self {
            chromosomes,
            generation: generation.unwrap_or(0),
        }
    }
    /// Number of chromosomes in this population.
    pub fn len(&self) -> usize {
        self.chromosomes.len()
    }
    /// Whether this instance is empty.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
    /// Add a chromosome to this population.
    pub fn add_chromosome(&mut self, chromosome: Chromosome<G, F>) {
        self.chromosomes.push(chromosome);
    }
    /// Get the chromosome at the given index.
    pub fn get_chromosome(&self, index: usize) -> &Chromosome<G, F> {
        self.chromosomes.get(index).unwrap()
    }
    /// Delete the chromosome at the given index.
    pub fn del_chromosome(&mut self, index: usize) -> Chromosome<G, F> {
        self.chromosomes.remove(index)
    }
    /// Get all fitness values in the population and store them in a vector. Panics if
    /// some values are not yet calculated.
    pub fn fitness(&self) -> (impl Iterator<Item = &F> + '_) {
        self.chromosomes.iter().map(|c| c.fitness.as_ref().unwrap())
    }
}

/// A Hall Of Fame contains the fittest unique chromosomes.
#[derive(Clone, Debug, Default)]
pub struct HallOfFame<G: Gene, F: Fitness> {
    /// Chromosomes in the Hall Of Fame. From best to worst.
    pub chromosomes: Vec<Chromosome<G, F>>,
    /// Unique gene sequences (Chromosomes) in the Hall Of Fame.
    pub uniques: HashSet<Vec<G>>,
    /// Maximum Hall Of Fame capacity. Defaults to 1 (the single best chromosome).
    pub capacity: usize,
}
impl<G: Gene, F: Fitness> HallOfFame<G, F> {
    /// Create a new Hall Of Fame.
    pub fn new(capacity: Option<usize>) -> Self {
        Self {
            chromosomes: vec![],
            uniques: HashSet::<Vec<G>>::new(),
            capacity: capacity.unwrap_or(1),
        }
    }
    /// The current number of chromosomes in this Hall Of Fame.
    pub fn len(&self) -> usize {
        self.chromosomes.len()
    }
    /// Whether this population is empty.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
    /// Try adding a candidate chromosome to the Hall Of Fame.
    pub fn hall_of_fame_one(&mut self, candidate: &Chromosome<G, F>) -> Option<usize> {
        let size = self.len().min(self.capacity);
        let mut place = size;
        let fitness = candidate.fitness.as_ref().unwrap();

        // Move up if we're better.
        while place > 0
            && fitness
                > self
                    .chromosomes
                    .get(place - 1)
                    .unwrap()
                    .fitness
                    .as_ref()
                    .unwrap()
        {
            place -= 1;
        }

        // Add if we're within capacity and a new unique addition.
        if place < self.capacity && self.uniques.insert(candidate.genes.clone()) {
            let chromo: Chromosome<G, F> = candidate.clone();
            self.chromosomes.insert(place, chromo);
            for i in place..self.len() {
                self.uniques.remove(&self.chromosomes.get(i).unwrap().genes);
            }
            self.chromosomes.truncate(self.capacity);
            return Some(place);
        }
        None
    }
    /// Update the Hall Of Fame with all chromosomes from another population.
    pub fn hall_of_fame_pop(&mut self, population: &Population<G, F>) -> usize {
        let mut added = 0;
        for candidate in population.chromosomes.iter() {
            match self.hall_of_fame_one(candidate) {
                Some(_) => added += 1,
                None => continue,
            };
        }
        added
    }
    /// Return the single best chromosome.
    pub fn best(&self) -> Option<&Chromosome<G, F>> {
        self.chromosomes.first()
    }
}

/// The lineage in a genetic algorithm is where the data resides. It keeps track of past
/// populations of chromosomes (generations) in an efficient way and also of the most
/// performant chromosomes in all past populations in the hall of fame.
#[derive(Clone, Debug, Default)]
pub struct Lineage<G: Gene, F: Fitness, R: Record> {
    /// Past generations. Up to `n_generations` are kept.
    pub generations: VecDeque<Population<G, F>>,
    /// Records of past generations. Up to `n_records` are kept.
    pub records: VecDeque<R>,
    /// Hall of Fame of best performing chromosomes.
    pub hall_of_fame: HallOfFame<G, F>,
    /// Optional capacity for the generations VecDeque.
    pub n_generations: Option<usize>,
    /// Optional capacity for the records VecDeque.
    pub n_records: Option<usize>,
}
impl<G: Gene, F: Fitness, R: Record> Lineage<G, F, R> {
    /// Create a new lineage instance with the given limits.
    pub fn new(
        n_generations: Option<usize>,
        n_records: Option<usize>,
        n_hall_of_fame: Option<usize>,
    ) -> Self {
        Self {
            generations: VecDeque::<Population<G, F>>::new(),
            n_generations,
            records: VecDeque::<R>::new(),
            n_records,
            hall_of_fame: HallOfFame::new(n_hall_of_fame),
        }
    }
    /// Create a new lineage with a starting population.
    pub fn from_population(
        population: Population<G, F>,
        evaluator: &mut dyn Evaluator<Gene = G, Fitness = F>,
        recorder: &mut dyn Recorder<Gene = G, Fitness = F, Record = R>,
        n_generations: Option<usize>,
        n_records: Option<usize>,
        n_hall_of_fame: Option<usize>,
    ) -> Self {
        let mut lineage = Self::new(n_generations, n_records, n_hall_of_fame);
        lineage.add_generation(population, evaluator, recorder);
        lineage
    }
    /// The current number of generations in this lineage.
    pub fn len(&self) -> usize {
        self.generations.len()
    }
    /// Whether this lineage is empty.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
    /// Add a population to this lineage. Evaluates fitness, records statistics and adds
    /// the population to the lineage's generations.
    pub fn add_generation(
        &mut self,
        mut population: Population<G, F>,
        evaluator: &mut dyn Evaluator<Gene = G, Fitness = F>,
        recorder: &mut dyn Recorder<Gene = G, Fitness = F, Record = R>,
    ) {
        evaluator.evaluate_pop(&mut population);
        self.hall_of_fame.hall_of_fame_pop(&population);

        self.records.push_front(recorder.record_pop(&population));
        if let Some(n_records) = self.n_records {
            self.records.truncate(n_records);
        }

        self.generations.push_front(population);
        if let Some(n_generations) = self.n_generations {
            self.generations.truncate(n_generations);
        }
    }
    /// Current population.
    pub fn current(&self) -> &Population<G, F> {
        self.generations.front().unwrap()
    }
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    use rand::prelude::*;
    use rand_chacha::ChaCha8Rng;

    #[allow(unused_imports)]
    use super::*;
    fn get_fixed_rng() -> ChaCha8Rng {
        ChaCha8Rng::seed_from_u64(0)
    }

    #[test]
    fn smoke_hall_of_fame() {
        let mut rng = get_fixed_rng();
        let mut chromosomes: Vec<Chromosome<usize, f64>> = vec![];
        for i in 0..10 {
            chromosomes.push({
                Chromosome {
                    genes: vec![i],
                    fitness: Some(rng.gen::<f64>()),
                }
            });
        }
        let capacity = 4;
        let mut hof = HallOfFame::<usize, f64>::new(Some(capacity));
        for (idx, chromo) in chromosomes.iter().enumerate() {
            hof.hall_of_fame_one(chromo);
            assert_eq!(
                hof.chromosomes.len(),
                (idx + 1).min(capacity),
                "HoF should grow up to size."
            );
        }

        // Assert fitness is descending.
        let mut ite = hof.chromosomes.iter();
        let mut last = ite.next().unwrap();
        for cur in ite {
            assert!(cur.genes != last.genes, "Genes can't be identical.");
            assert!(
                cur.fitness.unwrap() <= last.fitness.unwrap(),
                "Subsequent fitness should be lower or equal."
            );
            last = cur
        }
    }
}
