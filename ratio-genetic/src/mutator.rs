//! # Mutation procedures
//!
//! Mutations is the slight modification of chromosomes that aims to introduce some
//! variance into a given input population. This helps to keep the algorithm from
//! converging too quickly.

use std::marker::PhantomData;

use rand::prelude::*;

use crate::alias::{Fitness, Gene};
use crate::lineage::{Chromosome, Population};

/// Capability to mutate a single gene slice into something different.
pub trait Mutator<R: Rng + ?Sized>
where
    Self::Gene: Gene,
    Self::Fitness: Fitness,
{
    type Gene;
    type Fitness;

    /// Mutate a single gene slice in place.
    ///
    /// # Arguments
    ///
    /// * `rng` - Random number generator.
    /// * `genes` - Gene sequence to provide a mutated copy for.
    fn mutate_genes(&mut self, rng: &mut R, genes: &mut [Self::Gene]);
    /// Mutate a single chromosome in place.
    ///
    /// # Arguments
    ///
    /// * `rng` - Random number generator.
    /// * `chromosome` - Chromosome to mutate.
    fn mutate_one(&mut self, rng: &mut R, chromosome: &mut Chromosome<Self::Gene, Self::Fitness>) {
        self.mutate_genes(rng, &mut chromosome.genes);
    }
    /// Pre-process the input to set parameters on self.
    fn mutate_pop_pre(
        &mut self,
        #[allow(unused_variables)] population: &Population<Self::Gene, Self::Fitness>,
        #[allow(unused_variables)] n_chromosomes: Option<usize>,
        #[allow(unused_variables)] p_mutation: f64,
    ) {
    }
    /// Generate a new offspring by mutating individuals from a population.
    ///
    /// # Arguments
    ///
    /// * `rng` - Random number generator.
    /// * `population` - Population to mutate some chromosomes from.
    /// * `n_chromosomes` - Desired population size. If None, keep size identical to the
    ///   input population.
    /// * `p_mutation` - Probability for a chromosome to be mutated.
    ///
    /// # Returns
    ///
    /// A new population with potentially mutated chromosomes. Can contain duplicates.
    fn mutate_pop(
        &mut self,
        rng: &mut R,
        population: &Population<Self::Gene, Self::Fitness>,
        n_chromosomes: Option<usize>,
        p_mutation: f64,
    ) -> Population<Self::Gene, Self::Fitness> {
        self.mutate_pop_pre(population, n_chromosomes, p_mutation);
        let size = n_chromosomes.unwrap_or_else(|| population.len());
        let mut offspring = Population::default();
        let mut iter = 0..population.len();
        while offspring.len() < size {
            if let Some(i) = iter.next() {
                let mut chromosome = population.get_chromosome(i).to_owned();
                if p_mutation > rng.gen() {
                    self.mutate_one(rng, &mut chromosome);
                }
                offspring.add_chromosome(chromosome);
            } else {
                iter = 0..population.len();
            }
        }
        offspring
    }
}

/// Swap mutation by independently checking each position to be swapped with a random
/// other.
///
/// # Arguments
///
/// * `rng` - Random number generator.
/// * `slice` - Slice to be mutated.
/// * `p_swap` - Probability that a position is swapped.
pub fn mutate_swap<T, R: Rng + ?Sized>(rng: &mut R, slice: &mut [T], p_swap: f64) {
    let size = slice.len();
    let options = size - 1;
    for i in 0..size {
        if p_swap < rng.gen() {
            continue;
        }
        let swap = rng.gen_range(0..options);
        if swap < i {
            slice.swap(i, swap);
        } else {
            slice.swap(i, swap + 1);
        }
    }
}
/// Swap mutator by independently checking each position to be swapped with a random
/// other. See [`mutate_swap`] for the inner workings.
#[derive(Clone, Debug, Default)]
pub struct MutatorSwap<G: Gene, F: Fitness> {
    /// Probability with which each gene can initiate a swap within a chromosome.
    pub p_swap: f64,
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness> MutatorSwap<G, F> {
    pub fn new(p_swap: f64) -> Self {
        Self {
            p_swap,
            _phantom: PhantomData,
        }
    }
}
impl<Rnd: Rng + ?Sized, G: Gene, F: Fitness> Mutator<Rnd> for MutatorSwap<G, F> {
    type Gene = G;
    type Fitness = F;

    fn mutate_genes(&mut self, rng: &mut Rnd, genes: &mut [Self::Gene]) {
        mutate_swap(rng, genes, self.p_swap);
    }
}
