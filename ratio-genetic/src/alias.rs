//! # Common type/trait aliases.

use std::hash::Hash;

pub trait Gene: Clone + Default + Eq + Hash {}
impl<T: Clone + Default + Eq + Hash> Gene for T {}

pub trait Fitness: Clone + Default + PartialOrd + PartialEq {}
impl<T: Clone + Default + PartialOrd + PartialEq> Fitness for T {}

pub trait Record: Default {}
impl<T: Default> Record for T {}
