//! # Crossover procedures
//!
//! Crossover is the evolution stage that mimics reproduction of chromosomes that are
//! selected as parents (usually by the selection stage). Most methods assume that the
//! order of chromosomes in the input population is already randomized such that
//! crossover just iterate over any given pair in the current order of the input
//! population. Applying crossover usually takes parts of one parent and fills in the
//! gaps according to the characteristics of the other. They differ in ways of taking
//! and filling in the gaps.
//!
//! The main difficulty in "copy-pasting" is that the output usually still needs to
//! constitute a valid sequence that includes all indices up to the Chromosome's given
//! length.

use std::collections::HashSet;
use std::marker::PhantomData;

use num_traits::{AsPrimitive, FromPrimitive};
use rand::prelude::*;

use crate::alias::{Fitness, Gene};
use crate::lineage::{Chromosome, Population};

/// Capability to crossover two gene slices or apply crossover to a complete population.
pub trait Crossover<R: Rng + ?Sized>
where
    Self::Gene: Gene,
    Self::Fitness: Fitness,
{
    type Gene;
    type Fitness;

    /// Crossover one pair of gene slices into an offspring vec.
    fn crossover_genes(
        &mut self,
        rng: &mut R,
        parent_a: &[Self::Gene],
        parent_b: &[Self::Gene],
    ) -> (Vec<Self::Gene>, Vec<Self::Gene>);
    /// Crossover one pair of chromosomes into an offspring chromosome.
    #[allow(clippy::type_complexity)]
    fn crossover_pair(
        &mut self,
        rng: &mut R,
        parent_a: &Chromosome<Self::Gene, Self::Fitness>,
        parent_b: &Chromosome<Self::Gene, Self::Fitness>,
    ) -> (
        Chromosome<Self::Gene, Self::Fitness>,
        Chromosome<Self::Gene, Self::Fitness>,
    ) {
        let pair = self.crossover_genes(rng, parent_a.as_slice(), parent_b.as_slice());
        (Chromosome::new(pair.0), Chromosome::new(pair.1))
    }

    /// Pre-process the input to set parameters on self.
    fn crossover_pop_pre(
        &mut self,
        #[allow(unused_variables)] population: &Population<Self::Gene, Self::Fitness>,
        #[allow(unused_variables)] n_chromosomes: Option<usize>,
        #[allow(unused_variables)] p_crossover: f64,
    ) {
    }

    /// Generate a new offspring by crossing over individuals from a population.
    ///
    /// # Arguments
    ///
    /// * `rng` - Random number generator.
    /// * `population` - Population to select chromosomes from.
    /// * `n_chromosomes` -  Desired population size. If None, keep size identical to the input population.
    /// * `p_crossover` - Probability for a pair of chromosomes to crossover. Defaults to 1.0.
    ///
    /// # Returns
    ///
    /// A new population with potentially crossed chromosomes (cloned). Can contain duplicates.
    fn crossover_pop(
        &mut self,
        rng: &mut R,
        population: &Population<Self::Gene, Self::Fitness>,
        n_chromosomes: Option<usize>,
        p_crossover: f64,
    ) -> Population<Self::Gene, Self::Fitness> {
        self.crossover_pop_pre(population, n_chromosomes, p_crossover);
        let size = n_chromosomes.unwrap_or_else(|| population.len());
        let mut offspring = Population::default();
        // Pairwise iterator
        let mut iter = (1..population.len()).step_by(2);
        while offspring.len() < size {
            if let Some(i) = iter.next() {
                let parent_a = population.get_chromosome(i - 1);
                let parent_b = population.get_chromosome(i);
                if p_crossover < rng.gen() {
                    offspring.add_chromosome(parent_a.to_owned());
                    offspring.add_chromosome(parent_b.to_owned());
                } else {
                    let pair = self.crossover_pair(rng, parent_a, parent_b);
                    offspring.add_chromosome(pair.0);
                    offspring.add_chromosome(pair.1);
                }
            } else {
                // If iterator was exhausted, recreate it.
                iter = (1..population.len()).step_by(2);
            }
        }
        offspring
    }
}

/// Independent position crossover method (IPX)
///
/// Returns one randomly generated chromosome that is a positional selection from A,
/// with the remaining gaps filled in the order in which the genes occur in B. Each
/// position in A has an independent 50% chance to be selected.
pub fn crossover_genes_ipx<Rnd: Rng + ?Sized, G: Gene>(
    rng: &mut Rnd,
    parent_a: &[G],
    parent_b: &[G],
) -> (Vec<G>, Vec<G>) {
    let size = parent_a.len();
    let mut offspring_a: Vec<G> = vec![G::default(); size];
    let mut offspring_b: Vec<G> = vec![G::default(); size];
    let mut picked_places: Vec<bool> = vec![false; size];
    let mut picked_values_a: HashSet<G> = HashSet::<G>::new();
    let mut picked_values_b: HashSet<G> = HashSet::<G>::new();

    // For each position, we first roll the dice whether we take it from A, or don't.
    for i in 0..size {
        if rng.gen() {
            continue;
        }
        let (value_a, value_b) = (parent_a[i].to_owned(), parent_b[i].to_owned());
        offspring_a[i] = value_a.to_owned();
        offspring_b[i] = value_b.to_owned();
        // Bookkeeping for finished places and values.
        picked_places[i] = true;
        picked_values_a.insert(value_a);
        picked_values_b.insert(value_b);
    }

    // Fill in the remaining places with the values as they appear in B.
    let mut place = 0;
    for value in parent_b.iter() {
        if !picked_values_a.insert(value.to_owned()) {
            // Already picked this value, skip.
            continue;
        }
        while picked_places[place] {
            // Move to first unpicked place.
            place += 1;
        }
        offspring_a[place] = value.to_owned();
        place += 1;
    }
    // Fill in the remaining places with the values as they appear in A.
    place = 0;
    for value in parent_a.iter() {
        if !picked_values_b.insert(value.to_owned()) {
            // Already picked this value, skip.
            continue;
        }
        while picked_places[place] {
            // Move to first unpicked place.
            place += 1;
        }
        offspring_b[place] = value.to_owned();
        place += 1;
    }

    (offspring_a, offspring_b)
}

/// Independent position crossover (IPX)
///
/// See [`crossover_genes_ipx`] for the inner workings.
#[derive(Clone, Debug, Default)]
pub struct CrossoverIPX<G: Gene, F: Fitness> {
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness> CrossoverIPX<G, F> {
    pub fn new() -> Self {
        Self::default()
    }
}
impl<Rnd: Rng + ?Sized, G: Gene, F: Fitness> Crossover<Rnd> for CrossoverIPX<G, F> {
    type Gene = G;
    type Fitness = F;
    fn crossover_genes(
        &mut self,
        rng: &mut Rnd,
        parent_a: &[Self::Gene],
        parent_b: &[Self::Gene],
    ) -> (Vec<Self::Gene>, Vec<Self::Gene>) {
        crossover_genes_ipx(rng, parent_a, parent_b)
    }
}

/// Single point crossover
///
/// Slices the vectors at position X and returns two new vectors with the heads of one
/// and tails of another.
pub fn crossover_genes_point<Rnd: Rng + ?Sized, G: Gene>(
    rng: &mut Rnd,
    parent_a: &[G],
    parent_b: &[G],
) -> (Vec<G>, Vec<G>) {
    let size = parent_a.len().min(parent_b.len());
    let point = rng.gen_range(1..size - 1); // Always crossover something.
    let offspring_a = [&parent_a[0..point], &parent_b[point..]].concat();
    let offspring_b = [&parent_b[0..point], &parent_a[point..]].concat();
    (offspring_a, offspring_b)
}

/// Single point crossover
///
/// See [`crossover_genes_point`] for the inner workings.
#[derive(Clone, Debug, Default)]
pub struct CrossoverPoint<G: Gene, F: Fitness> {
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness> CrossoverPoint<G, F> {
    pub fn new() -> Self {
        Self::default()
    }
}
impl<Rnd: Rng + ?Sized, G: Gene, F: Fitness> Crossover<Rnd> for CrossoverPoint<G, F> {
    type Gene = G;
    type Fitness = F;
    fn crossover_genes(
        &mut self,
        rng: &mut Rnd,
        parent_a: &[Self::Gene],
        parent_b: &[Self::Gene],
    ) -> (Vec<Self::Gene>, Vec<Self::Gene>) {
        crossover_genes_point(rng, parent_a, parent_b)
    }
}

/// Blending crossover for float-like valued gene sequences
///
/// Blends both parents' values to achieve offspring. Note that alpha pulls "further"
/// than the domain between the two parents for a 1.0 value.
///
/// Each value is determined by:
/// `gamma * A + (1 - gamma)`, where `gamma = (1.0 + 2.0 * alpha) * rng.gen() - alpha`
///
/// This means that the interval is extended past regular averaging for any `alpha > 0.0`.
pub fn crossover_genes_blend<Rnd: Rng + ?Sized, G: Gene + AsPrimitive<f64> + FromPrimitive>(
    rng: &mut Rnd,
    parent_a: &[G],
    parent_b: &[G],
    alpha: f64,
) -> (Vec<G>, Vec<G>) {
    let mut offspring_a: Vec<G> = vec![G::from_f64(0.0).unwrap(); parent_a.len()];
    let mut offspring_b: Vec<G> = vec![G::from_f64(0.0).unwrap(); parent_b.len()];
    let factor = 1.0 + 2.0 * alpha;
    for (i, (a, b)) in parent_a.iter().zip(parent_b.iter()).enumerate() {
        let gamma: f64 = factor * rng.gen::<f64>() - alpha;
        offspring_a[i] = G::from_f64(gamma * a.as_() + (1.0 - gamma) * b.as_()).unwrap();
        offspring_b[i] = G::from_f64((1.0 - gamma) * a.as_() + gamma * b.as_()).unwrap();
    }
    (offspring_a, offspring_b)
}

/// Blend crossover for float-like gene sequences
///
/// See [`crossover_genes_blend`] for the inner workings.
#[derive(Clone, Debug, Default)]
pub struct CrossoverBlend<G: Gene, F: Fitness> {
    alpha: f64,
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness> CrossoverBlend<G, F> {
    pub fn new(alpha: f64) -> Self {
        Self {
            alpha,
            _phantom: PhantomData,
        }
    }
}
impl<
        Rnd: Rng + ?Sized,
        G: Gene + num_traits::FromPrimitive + num_traits::AsPrimitive<f64>,
        F: Fitness,
    > Crossover<Rnd> for CrossoverBlend<G, F>
{
    type Gene = G;
    type Fitness = F;
    fn crossover_genes(
        &mut self,
        rng: &mut Rnd,
        parent_a: &[Self::Gene],
        parent_b: &[Self::Gene],
    ) -> (Vec<Self::Gene>, Vec<Self::Gene>) {
        crossover_genes_blend(rng, parent_a, parent_b, self.alpha)
    }
}

/// Simulated binary crossover for float-like valued gene sequences
///
/// `eta` is the crowding degree of the crossover. The higher the eta, the more
/// the offspring resembles its parents.
pub fn crossover_genes_simulated_binary<
    Rnd: Rng + ?Sized,
    G: Gene + AsPrimitive<f64> + FromPrimitive,
>(
    rng: &mut Rnd,
    parent_a: &[G],
    parent_b: &[G],
    eta: f64,
) -> (Vec<G>, Vec<G>) {
    let mut offspring_a: Vec<G> = vec![G::from_f64(0.0).unwrap(); parent_a.len()];
    let mut offspring_b: Vec<G> = vec![G::from_f64(0.0).unwrap(); parent_b.len()];
    for (i, (a, b)) in parent_a.iter().zip(parent_b.iter()).enumerate() {
        let (a, b) = (a.as_(), b.as_());
        let r = rng.gen::<f64>();
        let mut beta = if r < 0.5 {
            2.0 * r
        } else {
            1.0 / (2.0 * (1.0 - r))
        };
        beta = beta.powf(1.0 / (eta + 1.));
        offspring_a[i] = G::from_f64(0.5 * (((1.0 + beta) * a) + ((1.0 - beta) * b))).unwrap();
        offspring_b[i] = G::from_f64(0.5 * (((1.0 - beta) * a) + ((1.0 + beta) * b))).unwrap();
    }
    (offspring_a, offspring_b)
}

/// Simulated binary crossover for float-like valued gene sequences
///
/// See [`crossover_genes_simulated_binary`] for the inner workings.
#[derive(Clone, Debug, Default)]
pub struct CrossoverSimulatedBinary<G: Gene, F: Fitness> {
    eta: f64,
    _phantom: PhantomData<(G, F)>,
}
impl<G: Gene, F: Fitness> CrossoverSimulatedBinary<G, F> {
    pub fn new(eta: f64) -> Self {
        Self {
            eta,
            _phantom: PhantomData,
        }
    }
}
impl<
        Rnd: Rng + ?Sized,
        G: Gene + num_traits::FromPrimitive + num_traits::AsPrimitive<f64>,
        F: Fitness,
    > Crossover<Rnd> for CrossoverSimulatedBinary<G, F>
{
    type Gene = G;
    type Fitness = F;
    fn crossover_genes(
        &mut self,
        rng: &mut Rnd,
        parent_a: &[Self::Gene],
        parent_b: &[Self::Gene],
    ) -> (Vec<Self::Gene>, Vec<Self::Gene>) {
        crossover_genes_simulated_binary(rng, parent_a, parent_b, self.eta)
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    #[allow(unused_imports)]
    use pretty_assertions::{assert_eq, assert_ne, assert_str_eq};
    use rand_chacha::ChaCha8Rng;

    #[allow(unused_imports)]
    use super::*;
    use crate::generator::{Generator, GeneratorRandomSequence};
    fn get_fixed_rng() -> ChaCha8Rng {
        ChaCha8Rng::seed_from_u64(0)
    }

    #[test]
    fn smoke_crossover() {
        let mut rng = get_fixed_rng();
        let mut crossover = CrossoverIPX::new();
        let a = Chromosome::<usize, usize>::new((0..10).collect());
        let b = Chromosome::<usize, _>::new((0..10).rev().collect());
        let (copy_a, _) = crossover.crossover_pair(&mut rng, &a, &a);
        assert_eq!(
            &copy_a.genes, &a.genes,
            "Offspring of 'a' with itself should be identical."
        );

        let (c_ab, _) = crossover.crossover_pair(&mut rng, &a, &b);
        let (c_ba, _) = crossover.crossover_pair(&mut rng, &b, &a);
        assert_eq!(&c_ab.genes, &vec![8, 7, 2, 5, 4, 3, 6, 1, 0, 9]);
        assert_eq!(&c_ba.genes, &vec![9, 0, 3, 4, 5, 6, 7, 2, 1, 8]);

        let mut generator = GeneratorRandomSequence::<usize, _>::default();
        let population = generator.generate_pop(&mut rng, 10, 20);
        let offspring = crossover.crossover_pop(&mut rng, &population, None, 0.5);
        assert_eq!(&offspring.len(), &population.len());
        assert_ne!(&offspring.chromosomes, &population.chromosomes);

        let double =
            crossover.crossover_pop(&mut rng, &population, Some(2 * population.len()), 0.0);
        assert_eq!(double.len(), 2 * offspring.len());
    }

    #[test]
    fn smoke_ipx() {
        let mut rng = get_fixed_rng();
        let size = 6;
        let parent_a: Vec<usize> = (0..size).collect();
        let parent_b: Vec<usize> = parent_a.iter().rev().copied().collect();

        let (c0, _) = crossover_genes_ipx(&mut rng, &parent_a, &parent_a);
        assert_eq!(
            &c0, &parent_a,
            "Child from identical parents should be identical, too."
        );

        // For chromosomes of 20 length, getting different ones shouldn't be this hard.
        let (c1, _) = crossover_genes_ipx(&mut rng, &parent_a, &parent_b);
        // Element origin:  B  A  A  B  A  B
        assert_eq!(c1, vec![5, 1, 2, 4, 3, 0], "Expected result for fixed RNG.");

        let (c2, _) = crossover_genes_ipx(&mut rng, &parent_a, &parent_b);
        assert_ne!(c1, c2, "Children shouldn't be equal.");

        let unique1: HashSet<usize> = HashSet::from_iter(c1.iter().cloned());
        let unique2: HashSet<usize> = HashSet::from_iter(c2.iter().cloned());
        assert_eq!(
            unique1.len(),
            size,
            "Uniqueness of indices should be preserved."
        );
        assert_eq!(
            unique2.len(),
            size,
            "Uniqueness of indices should be preserved."
        );
    }

    #[test]
    fn smoke_discrete_crossover_methods() {
        let mut rng = get_fixed_rng();
        let mut generator = GeneratorRandomSequence::<usize, _>::default();
        let population = generator.generate_pop(&mut rng, 33, 100);
        type BoxedCrossover<R, Gene, Fitness> =
            Box<dyn Crossover<R, Gene = Gene, Fitness = Fitness>>;
        let mut methods: Vec<BoxedCrossover<_, usize, usize>> = vec![];
        methods.push(Box::new(CrossoverIPX::new()));
        methods.push(Box::new(CrossoverPoint::new()));
        for m in methods.iter_mut() {
            let offspring = m.crossover_pop(&mut rng, &population, None, 0.5);
            assert_eq!(offspring.len(), population.len());
            assert_ne!(offspring, population);
        }
    }

    #[test]
    fn smoke_float_crossover_methods() {
        let mut rng = get_fixed_rng();
        let mut generator = GeneratorRandomSequence::default();
        let population: Population<isize, f64> = generator.generate_pop(&mut rng, 33, 100);
        type BoxedCrossover<R, Gene, Fitness> =
            Box<dyn Crossover<R, Gene = Gene, Fitness = Fitness>>;
        let mut methods: Vec<BoxedCrossover<_, isize, f64>> = vec![];
        methods.push(Box::new(CrossoverBlend::new(0.2)));
        methods.push(Box::new(CrossoverSimulatedBinary::new(0.1)));
        for m in methods.iter_mut() {
            let offspring = m.crossover_pop(&mut rng, &population, None, 0.5);
            assert_eq!(offspring.len(), population.len());
            assert_ne!(offspring, population);
        }
    }
}
