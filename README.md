# Ratio Genetic algorithms

Genetic algorithm library with a focus on the field of Dependency Structure Matrix (DSM)
analysis. Currently in a very early state! Minor versions should be treated as major
breaking changes. Therefore, make sure to pin your versions accordingly.

The library is setup according to the [Strategy pattern](https://rust-unofficial.github.io/patterns/patterns/behavioural/strategy.html)

    The basic idea behind the Strategy pattern is that, given an algorithm solving a
    particular problem, we define only the skeleton of the algorithm at an abstract
    level, and we separate the specific algorithm’s implementation into different parts.

This makes it especially useful to create your very own specific implementation of a
genetic algorithm while having some batteries included implementations to get up and
running quickly.

# Documentation

Please refer to the crate's documentation on [docs.rs](https://docs.rs/ratio-genetic)
for more information on it's usage.

# Contributions

Contributions (and issues) are more than welcome! Fork it, work it, and make a PR!
Please make sure you try to keep `cargo clippy` happy. And include some basic tests as
examples or tests at the bottom.

To get going easily, please install [just](https://github.com/casey/just) and list the
available commands by typing `just` right in the project folder.

If you have Docker installed and would like to manage the project using a Docker image:
run a `just pull-docker`. You can run any just command afterwards using
`just in-docker <cmd>`, like `just in-docker test` to run all tests.

# Changelog

This project keeps a changelog in [CHANGELOG.md](./CHANGELOG.md) following the
[Keep a Changelog](https://keepachangelog.com/en/1.1.0/) format.
