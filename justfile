IMAGE := env("IMAGE", "registry.gitlab.com/ratio-case-os/docker/rust-ci")
TAG := env("TAG", "cross")
TARGETS := env("TARGETS", "x86_64-unknown-linux-gnu x86_64-apple-darwin x86_64-pc-windows-msvc aarch64-unknown-linux-gnu aarch64-apple-darwin aarch64-pc-windows-msvc")
FLAMES := env("FLAMES", "simple::tests::smoke_sequencing")
HERE := env("HERE", justfile_directory())
FLAMES_DIR := env("FLAMES_DIR", HERE + "/target/flames")
PYTHON_DIR := env("PYTHON_DIR", HERE + "/ratio-genetic-py")
WHEELS_DIR := env("WHEELS_DIR", HERE + "/target/wheels")
PYPI_TOKEN := env("PYPI_TOKEN", "")

# Show the recipe list.
default:
  @just --list

# Clean ignored files.
clean:
  cargo clean
  rm -r {{PYTHON_DIR}}/.venv/ || true
  rm {{PYTHON_DIR}}/**/*.so || true

# Push the tools container image to the registry.
pull-container:
  podman pull {{IMAGE}}:{{TAG}}

# Push the tools container image to the registry.
push-container:
  podman push {{IMAGE}}:{{TAG}}

# Run the tools container image with the project directory mounted.
run-container args="" cmd="":
  podman run --rm -it --userns=keep-id -v {{HERE}}:/work:z {{args}} {{IMAGE}}:{{TAG}} {{cmd}}

# Run a recipe in the tools container image.
in-container recipe="":
  podman run --rm -it --userns=keep-id -v {{HERE}}:/work:z {{IMAGE}}:{{TAG}} just {{recipe}}

# Build package in release mode.
build: build-lib build-python

# Build Rust library for all available targets.
build-lib:
  cargo build -p ratio-genetic --release --all-targets --color always

# Build Python bindings for all set targets.
build-python: develop-python
  #!/bin/bash
  rm -rf {{WHEELS_DIR}};\
  for i in {{TARGETS}};\
  do poetry -C {{PYTHON_DIR}} run maturin build --quiet --release --zig --target "$i";\
  done

# Install Python package in development mode.
develop-python *args:
  poetry -C {{PYTHON_DIR}} install {{args}}; poetry -C {{PYTHON_DIR}} run maturin develop --quiet

# Enable perf for regular users. Probably need root rights to run this.
enable-perf:
  echo -1 | sudo tee /proc/sys/kernel/perf_event_paranoid

# Create flamegraph of certain scripts for performance measurement.
flamegraph:
  #!/bin/bash
  mkdir -p {{FLAMES_DIR}}
  for i in {{FLAMES}}; \
  do CARGO_PROFILE_RELEASE_DEBUG=true cargo flamegraph -o "{{FLAMES_DIR}}/$i.svg" --no-inline --unit-test -- "$i" --color always || true; \
  done
  rm -f perf.data
  rm -f perf.data.old

# Lint both the library and Python bindings.
lint fix="": (lint-lib fix) (lint-python fix)

# Lint the Rust code.
lint-lib fix="":
  cargo clippy --all-targets -p ratio-genetic --color always {{ if fix == "" { "" } else { "--fix --allow-dirty" } }}
  cargo +nightly fmt -p ratio-genetic {{ if fix == "" { "--check" } else { "" } }}

# Lint the Python bindings.
lint-python fix="": develop-python
  cargo clippy --all-targets -p ratio-genetic-py --color always {{ if fix == "" { "" } else { "--fix --allow-dirty" } }}
  cargo +nightly fmt -p ratio-genetic-py {{ if fix == "" { "--check" } else { "" } }}
  poetry -C {{PYTHON_DIR}} run black . {{ if fix == "" { "--check" } else { "" } }}

# Check for outdated packages for the library.
outdated-lib:
  cargo outdated --color always

# Build and publish both the library and Python bindings.
publish: publish-lib publish-python

# Build and publish the library.
publish-lib: build-lib
  cargo publish -p ratio-genetic --color always

# Build and publish the Python bindings.
@publish-python *args: build-python
  poetry -C {{PYTHON_DIR}} publish --dist-dir {{WHEELS_DIR}} --username __token__ --password {{PYPI_TOKEN}} {{args}}

# Run a single test cycle for both the library and the Python bindings.
test: test-lib-coverage test-python

# Run library tests continuously.
test-lib:
  bacon -j test -p ratio-genetic

# Run library tests with coverage.
test-lib-coverage:
  cargo tarpaulin -p ratio-genetic --color always

# Run Python bindings tests.
test-python: develop-python
  poetry -C {{PYTHON_DIR}} run pytest --cov --cov-report=term-missing -vvv -s

# Upgrade library dependencies, update Python bindings.
upgrade: upgrade-lib update-python

# Upgrade library dependencies.
upgrade-lib:
  cargo upgrade

# Update Python bindings.
update-python:
  poetry -C {{PYTHON_DIR}} update
