# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Minor version increments during 0.x.x should be treated as major versions.

## [0.4.0] - 2025-01-14

### Added

- Added a wheels built for MacOS and aarch64 for all three platforms (Linux, macOS, Windows) using Maturin.

### Changed

- Updated all project dependencies to recent versions:
  - Rust crates
  - Python dependencies
  - Maturin/PyO3 build tools
- Container image is now built using Podman, also bumped all dependencies here.

## [0.3.0] - 2023-02-15

### Added

- Added Python typings for the currently available bindings.
- Added some variants of operators found in literature.
- Improve Python bindings to adopt most of the current library.

## [0.2.2] - 2023-02-03

### Added

- Add a [HdrHistogram](https://docs.rs/hdrhistogram/latest/hdrhistogram/) Recorder implementation.

### Changed

- Consolidate excessive function documentation.

## [0.1.2] - 2023-02-03

### Fixed

- Repository name.

## [0.1.1] - 2023-02-03

### Fixed

- Packaging fixups (README, Cargo.toml).

## [0.1.0] - 2023-02-03

### Added

- Initial version.
