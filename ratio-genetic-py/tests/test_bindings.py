from random import randint

import pytest
from ratio_genetic_py import (
    Chromosome,
    ConvergenceKinds,
    CrossoverKinds,
    EvaluatorKinds,
    EvaluatorMatrix,
    FitnessStatistics,
    GeneratorKinds,
    MutatorSwap,
    Population,
    RecorderKinds,
    SelectorKinds,
    SequencingSettings,
    sequence_sga,
)


def test_types():
    Chromosome([], 0.0)
    Population([], 0)
    FitnessStatistics(0, 0.0, 1.0, 2.0, 3.0, 4.0)

    EvaluatorMatrix(EvaluatorKinds.FeedbackDistance, [], None)
    with pytest.raises(ValueError):
        EvaluatorMatrix(EvaluatorKinds.Value, [], None)
    assert len(EvaluatorMatrix.kinds()) == 3


def test_seq():
    dim = 100
    vec = [randint(0, 1) * randint(0, 5) for _ in range(dim * dim)]
    generator = GeneratorKinds.RandomSequence
    evaluator = EvaluatorMatrix(EvaluatorKinds.FeedbackDistance, vec, 1)
    recorder = RecorderKinds.FitnessStatistics
    selector = SelectorKinds.Roulette
    crossover = CrossoverKinds.IPX
    mutator = MutatorSwap(0.05)
    convergence = ConvergenceKinds.Never
    lineage = sequence_sga(
        SequencingSettings(
            n_genes=100,
            n_generations=100,
            n_chromosomes=100,
            n_hall_of_fame=1,
            p_crossover=0.3,
            p_mutation=0.03,
        ),
        generator,
        evaluator,
        recorder,
        selector,
        crossover,
        mutator,
        convergence,
    )
    assert len(lineage.hall_of_fame.chromosomes[0].genes) == 100
