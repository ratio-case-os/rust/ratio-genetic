use num_traits::AsPrimitive;
use pyo3::prelude::*;
use rand::Rng;
use ratio_genetic::alias::{Fitness, Gene};
use ratio_genetic::selector;

#[pyclass(eq, eq_int)]
#[derive(Clone, Debug, Default, PartialEq)]
pub enum SelectorKinds {
    #[default]
    Roulette,
    Random,
}
#[pymethods]
impl SelectorKinds {
    #[new]
    pub fn new() -> Self {
        Self::default()
    }
}

#[derive(Clone, Debug, FromPyObject)]
pub enum Selector {
    Kind(SelectorKinds),
}

pub type SelectorBox<R, Gene, Fitness> =
    Box<dyn selector::Selector<R, Gene = Gene, Fitness = Fitness>>;

impl<R: Rng + ?Sized, G: Gene + 'static, F: Fitness + AsPrimitive<f64> + 'static> TryFrom<Selector>
    for SelectorBox<R, G, F>
{
    type Error = PyErr;
    fn try_from(value: Selector) -> Result<Self, Self::Error> {
        Ok(match value {
            Selector::Kind(options) => match options {
                SelectorKinds::Random => Box::new(selector::SelectorRandom::new()),
                SelectorKinds::Roulette => Box::new(selector::SelectorRoulette::new()),
            },
        })
    }
}
