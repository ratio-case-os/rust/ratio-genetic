use num_traits::FromPrimitive;
use pyo3::prelude::*;
use rand::Rng;
use ratio_genetic::alias::{Fitness, Gene};
use ratio_genetic::generator;

/// Generator kinds. Primary use is for generators without further parameters.
#[pyclass(eq, eq_int)]
#[derive(Clone, Debug, Default, PartialEq)]
pub enum GeneratorKinds {
    #[default]
    /// Generate random sequences (no further options).
    RandomSequence,
}

/// The union of allowed arguments for the generator construction.
#[derive(Clone, Debug, FromPyObject)]
pub enum Generator {
    /// A sole generator kind (no further options).
    Kind(GeneratorKinds),
}
pub type GeneratorBox<R, Gene, Fitness> =
    Box<dyn generator::Generator<R, Gene = Gene, Fitness = Fitness>>;

impl<R: Rng + ?Sized, G: Gene + FromPrimitive + 'static, F: Fitness + 'static> TryFrom<Generator>
    for GeneratorBox<R, G, F>
{
    type Error = PyErr;
    fn try_from(value: Generator) -> Result<Self, Self::Error> {
        Ok(match value {
            Generator::Kind(options) => match options {
                GeneratorKinds::RandomSequence => {
                    Box::new(generator::GeneratorRandomSequence::new())
                }
            },
        })
    }
}
