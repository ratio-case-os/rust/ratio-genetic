use pyo3::exceptions::PyValueError;
use pyo3::prelude::*;
use rand::thread_rng;
use ratio_genetic::recorder::FitnessStatistics;
use ratio_genetic::simple;
use ratio_genetic::simple::SimpleGeneticAlgorithm;

use crate::convergence::{Convergence, ConvergenceBox};
use crate::crossover::{Crossover, CrossoverBox};
use crate::evaluator::{Evaluator, EvaluatorBox};
use crate::generator::{Generator, GeneratorBox};
use crate::lineage::Lineage;
use crate::mutator::{Mutator, MutatorBox};
use crate::recorder::{Recorder, RecorderBox};
use crate::selector::{Selector, SelectorBox};

/// Common settings for a simple genetic algorithm.
#[pyclass(get_all, set_all)]
#[derive(Debug, Default, FromPyObject)]
pub struct SequencingSettings {
    /// Number of genes in each chromosome to generate if needed.
    pub n_genes: usize,
    /// Number of chromosomes to strive for in each population. (should be even)
    pub n_chromosomes: Option<usize>,
    /// Maximum number of generations to evaluate.
    pub n_generations: Option<usize>,
    /// Maximum number of generation records to keep.
    pub n_records: Option<usize>,
    /// Number of best performing chromosomes to keep.
    pub n_hall_of_fame: Option<usize>,
    /// Probability of crossover happening between each pair of chromosomes.
    pub p_crossover: f64,
    /// Probability of mutation happening for each chromosome.
    pub p_mutation: f64,
}
#[pymethods]
impl SequencingSettings {
    #[new]
    #[pyo3(signature = (n_genes, p_crossover, p_mutation, n_chromosomes=None, n_generations=None, n_records=None, n_hall_of_fame=None))]
    pub fn new(
        n_genes: usize,
        p_crossover: f64,
        p_mutation: f64,
        n_chromosomes: Option<usize>,
        n_generations: Option<usize>,
        n_records: Option<usize>,
        n_hall_of_fame: Option<usize>,
    ) -> Self {
        Self {
            n_genes,
            n_chromosomes,
            n_generations,
            n_records,
            n_hall_of_fame,
            p_crossover,
            p_mutation,
        }
    }
}
impl From<SequencingSettings> for simple::SimpleGeneticSettings {
    fn from(value: SequencingSettings) -> Self {
        Self::new(
            Some(value.n_genes),
            value.n_chromosomes,
            value.n_generations,
            value.n_records,
            value.n_hall_of_fame,
            value.p_crossover,
            value.p_mutation,
        )
    }
}

#[pyfunction]
#[allow(clippy::too_many_arguments)]
pub fn sequence_sga(
    settings: SequencingSettings,
    generator: Generator,
    evaluator: Evaluator,
    recorder: Recorder,
    selector: Selector,
    crossover: Crossover,
    mutator: Mutator,
    convergence: Convergence,
) -> PyResult<Lineage> {
    if settings.n_generations.is_none() {
        // if let Convergence::Never { never } = operators.convergence {
        return Err(PyValueError::new_err("with no limit on n_generations and convergence set to never, this would run indefinitely."));
        // }
    }

    let mut generator: GeneratorBox<_, _, _> = generator.try_into()?;
    let mut evaluator: EvaluatorBox<_, _> = evaluator.try_into()?;
    let mut recorder: RecorderBox<_, _, _> = recorder.try_into()?;
    let mut selector: SelectorBox<_, _, _> = selector.try_into()?;
    let mut crossover: CrossoverBox<_, _, _> = crossover.try_into()?;
    let mut mutator: MutatorBox<_, _, _> = mutator.try_into()?;
    let mut convergence: ConvergenceBox<_, _, _> = convergence.try_into()?;
    let mut operators = simple::SimpleGeneticOperators::new(
        &mut *generator,
        &mut *evaluator,
        &mut *recorder,
        &mut *selector,
        &mut *crossover,
        &mut *mutator,
        &mut *convergence,
    );

    let parsed_settings = settings.into();

    struct Sga;
    impl simple::SimpleGeneticAlgorithm for Sga {
        type Gene = usize;
        type Fitness = f64;
        type Record = FitnessStatistics;
    }
    let sga = Sga {};

    let mut rng = thread_rng();
    let lineage = sga.execute(&mut rng, None, &parsed_settings, &mut operators);

    Ok(lineage.into())
}
