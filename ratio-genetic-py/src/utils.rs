use nalgebra::{DMatrix, Scalar};
use pyo3::exceptions::PyValueError;
use pyo3::PyResult;

pub fn vector_to_matrix<V>(vec: &Vec<V>) -> PyResult<DMatrix<V>>
where
    V: Scalar,
{
    let dim = (vec.len() as f64).sqrt() as usize;
    if dim * dim != vec.len() {
        return Err(PyValueError::new_err(format!(
            "Vector should be of size dimensions^2. {}*{} != {}",
            dim,
            dim,
            vec.len()
        )));
    }
    let matrix = DMatrix::<V>::from_row_slice(dim, dim, vec.as_slice());
    Ok(matrix)
}
