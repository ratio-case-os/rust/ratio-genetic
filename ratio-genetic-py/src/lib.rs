//! Python bindings for Ratio's genetic algorithms library
//!
//! Currently, only the sequencing application is implemented as a proof of concept.

pub mod convergence;
pub mod crossover;
pub mod evaluator;
pub mod generator;
pub mod lineage;
pub mod mutator;
pub mod recorder;
pub mod selector;
pub mod sequencing;
mod utils;

use convergence::*;
use crossover::*;
use evaluator::*;
use generator::*;
use lineage::*;
use mutator::*;
use pyo3::prelude::*;
use recorder::*;
use selector::*;
use sequencing::*;

/// A Python module implemented in Rust.
#[pymodule]
fn ratio_genetic_py(m: &Bound<'_, PyModule>) -> PyResult<()> {
    m.add_class::<Chromosome>()?;
    m.add_class::<Population>()?;
    m.add_class::<FitnessStatistics>()?;
    m.add_class::<HallOfFame>()?;
    m.add_class::<Lineage>()?;
    m.add_class::<ConvergenceKinds>()?;
    m.add_class::<CrossoverKinds>()?;
    m.add_class::<CrossoverBlend>()?;
    m.add_class::<CrossoverSimulatedBinary>()?;
    m.add_class::<EvaluatorKinds>()?;
    m.add_class::<EvaluatorMatrix>()?;
    m.add_class::<EvaluatorValue>()?;
    m.add_class::<GeneratorKinds>()?;
    m.add_class::<MutatorKinds>()?;
    m.add_class::<MutatorSwap>()?;
    m.add_class::<RecorderKinds>()?;
    m.add_class::<RecorderHdrHistogram>()?;
    m.add_class::<SelectorKinds>()?;
    m.add_class::<SequencingSettings>()?;
    m.add_function(wrap_pyfunction!(sequence_sga, m)?)?;
    Ok(())
}
