use pyo3::prelude::*;
use rand::Rng;
use ratio_genetic::alias::{Fitness, Gene};
use ratio_genetic::mutator;

#[pyclass(eq, eq_int)]
#[derive(Clone, Debug, Default, PartialEq)]
pub enum MutatorKinds {
    #[default]
    Swap,
}

#[pyclass]
#[derive(Clone, Debug, Default)]
pub struct MutatorSwap {
    #[allow(dead_code)]
    kind: MutatorKinds,
    pub p_swap: f64,
}
#[pymethods]
impl MutatorSwap {
    #[new]
    pub fn new(p_swap: f64) -> Self {
        Self {
            kind: MutatorKinds::Swap,
            p_swap,
        }
    }
}
impl<G: Gene, F: Fitness> From<MutatorSwap> for mutator::MutatorSwap<G, F> {
    fn from(value: MutatorSwap) -> Self {
        mutator::MutatorSwap::new(value.p_swap)
    }
}

#[derive(Clone, Debug, FromPyObject)]
pub enum Mutator {
    Swap(MutatorSwap),
}

pub type MutatorBox<R, Gene, Fitness> =
    Box<dyn mutator::Mutator<R, Gene = Gene, Fitness = Fitness>>;

impl<R: Rng + ?Sized, G: Gene + 'static, F: Fitness + 'static> TryInto<MutatorBox<R, G, F>>
    for Mutator
{
    type Error = PyErr;
    fn try_into(self) -> Result<MutatorBox<R, G, F>, Self::Error> {
        Ok(match self {
            Mutator::Swap(options) => Box::new(mutator::MutatorSwap::from(options)),
        })
    }
}
