use std::collections::HashSet;

use pyo3::prelude::*;
use ratio_genetic::{lineage, recorder};

use crate::recorder::FitnessStatistics;

/// A chromosome represented by a vector of genes and a fitness value.
#[pyclass(get_all, set_all)]
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Chromosome {
    /// The genes of this chromosome.
    pub genes: Vec<usize>,
    /// The fitness of this chromosome.
    pub fitness: Option<f64>,
}
#[pymethods]
impl Chromosome {
    /// A chromosome represented by a vector of genes and a fitness value.
    #[new]
    #[pyo3(signature = (genes, fitness=None))]
    pub fn new(genes: Vec<usize>, fitness: Option<f64>) -> Self {
        Self { genes, fitness }
    }
}
impl From<lineage::Chromosome<usize, f64>> for Chromosome {
    fn from(value: lineage::Chromosome<usize, f64>) -> Self {
        Self {
            genes: value.genes,
            fitness: value.fitness,
        }
    }
}

/// A population consisting of multiple chromosomes and a generation number.
#[pyclass(get_all, set_all)]
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Population {
    /// Chromosomes of this population.
    pub chromosomes: Vec<Chromosome>,
    /// Generation index.
    pub generation: usize,
}
#[pymethods]
impl Population {
    #[new]
    /// A population consisting of multiple chromosomes and a generation number.
    pub fn new(chromosomes: Vec<Chromosome>, generation: usize) -> Self {
        Self {
            chromosomes,
            generation,
        }
    }
}
impl From<lineage::Population<usize, f64>> for Population {
    fn from(value: lineage::Population<usize, f64>) -> Self {
        Self {
            chromosomes: value.chromosomes.into_iter().map(|x| x.into()).collect(),
            generation: value.generation,
        }
    }
}

/// A Hall Of Fame contains the fittest unique chromosomes.
#[pyclass(get_all, set_all)]
#[derive(Clone, Debug, Default)]
pub struct HallOfFame {
    /// Chromosomes in the Hall Of Fame. From best to worst.
    pub chromosomes: Vec<Chromosome>,
    /// Unique gene sequences (Chromosomes) in the Hall Of Fame.
    pub uniques: HashSet<Vec<usize>>,
    /// Maximum Hall Of Fame capacity. Defaults to 1 (the single best chromosome).
    pub capacity: usize,
}
#[pymethods]
impl HallOfFame {
    #[new]
    pub fn new(
        chromosomes: Vec<Chromosome>,
        uniques: HashSet<Vec<usize>>,
        capacity: usize,
    ) -> Self {
        Self {
            chromosomes,
            uniques,
            capacity,
        }
    }
}
impl From<lineage::HallOfFame<usize, f64>> for HallOfFame {
    fn from(value: lineage::HallOfFame<usize, f64>) -> Self {
        Self {
            chromosomes: value.chromosomes.into_iter().map(|x| x.into()).collect(),
            uniques: value.uniques,
            capacity: value.capacity,
        }
    }
}

#[pyclass(get_all, set_all)]
#[derive(Clone, Debug, Default)]
pub struct Lineage {
    /// Past generations. Up to `n_generations` are kept.
    pub generations: Vec<Population>,
    /// Records of past generations. Up to `n_records` are kept.
    pub records: Vec<FitnessStatistics>,
    /// Hall of Fame of best performing chromosomes.
    pub hall_of_fame: HallOfFame,
    /// Optional capacity for the generations VecDeque.
    pub n_generations: Option<usize>,
    /// Optional capacity for the records VecDeque.
    pub n_records: Option<usize>,
}
#[pymethods]
impl Lineage {
    #[new]
    #[pyo3(signature = (generations, records, hall_of_fame, n_generations=None, n_records=None))]
    pub fn new(
        generations: Vec<Population>,
        records: Vec<FitnessStatistics>,
        hall_of_fame: HallOfFame,
        n_generations: Option<usize>,
        n_records: Option<usize>,
    ) -> Self {
        Self {
            generations,
            records,
            hall_of_fame,
            n_generations,
            n_records,
        }
    }
}
impl From<lineage::Lineage<usize, f64, recorder::FitnessStatistics>> for Lineage {
    fn from(value: lineage::Lineage<usize, f64, recorder::FitnessStatistics>) -> Self {
        Self {
            generations: value.generations.into_iter().map(|x| x.into()).collect(),
            records: value.records.into_iter().map(|x| x.into()).collect(),
            hall_of_fame: value.hall_of_fame.into(),
            n_generations: value.n_generations,
            n_records: value.n_records,
        }
    }
}
