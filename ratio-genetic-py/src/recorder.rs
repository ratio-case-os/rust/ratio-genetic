use num_traits::AsPrimitive;
use pyo3::exceptions::PyValueError;
use pyo3::prelude::*;
use ratio_genetic::alias::{Fitness, Gene};
use ratio_genetic::recorder;

/// Fitness statistics record.
#[pyclass(get_all, set_all)]
#[derive(Clone, Debug, Default)]
pub struct FitnessStatistics {
    pub size: usize,
    pub min: f64,
    pub max: f64,
    pub sum: f64,
    pub mean: f64,
    pub variance: f64,
}
#[pymethods]
impl FitnessStatistics {
    #[new]
    pub fn new(size: usize, min: f64, max: f64, sum: f64, mean: f64, variance: f64) -> Self {
        Self {
            size,
            min,
            max,
            sum,
            mean,
            variance,
        }
    }
}
impl From<recorder::FitnessStatistics> for FitnessStatistics {
    fn from(value: recorder::FitnessStatistics) -> Self {
        Self {
            size: value.size,
            min: value.min,
            max: value.max,
            sum: value.sum,
            mean: value.mean,
            variance: value.variance,
        }
    }
}

#[pyclass(eq, eq_int)]
#[derive(Clone, Debug, Default, PartialEq)]
pub enum RecorderKinds {
    #[default]
    FitnessStatistics,
    HdrHistogram,
}
#[pymethods]
impl RecorderKinds {
    #[new]
    pub fn new() -> Self {
        Self::default()
    }
}

#[pyclass]
#[derive(Clone, Debug, Default)]
pub struct RecorderHdrHistogram {
    #[allow(dead_code)]
    kind: RecorderKinds,
    pub sigfig: u8,
}
#[pymethods]
impl RecorderHdrHistogram {
    #[new]
    pub fn new(sigfig: u8) -> Self {
        Self {
            kind: RecorderKinds::HdrHistogram,
            sigfig,
        }
    }
}
impl<G: Gene, F: Fitness + AsPrimitive<u64>> From<RecorderHdrHistogram>
    for recorder::RecorderHdrHistogram<G, F>
{
    fn from(value: RecorderHdrHistogram) -> Self {
        recorder::RecorderHdrHistogram::new(value.sigfig)
    }
}

#[derive(Clone, Debug, FromPyObject)]
pub enum Recorder {
    HdrHistogram(RecorderHdrHistogram),
    Kind(RecorderKinds),
}

pub type RecorderBox<Gene, Fitness, Record> =
    Box<dyn recorder::Recorder<Gene = Gene, Fitness = Fitness, Record = Record>>;

impl<G: Gene + 'static, F: Fitness + AsPrimitive<f64>>
    TryInto<RecorderBox<G, F, recorder::FitnessStatistics>> for Recorder
{
    type Error = PyErr;
    fn try_into(self) -> Result<RecorderBox<G, F, recorder::FitnessStatistics>, Self::Error> {
        #[allow(clippy::collapsible_match)]
        Ok(match self {
            Recorder::Kind(options) => match options {
                RecorderKinds::FitnessStatistics => {
                    Box::new(recorder::RecorderFitnessStatistics::new())
                }
                _ => return Err(PyValueError::new_err("unrecognized recorder options")),
            },
            _ => return Err(PyValueError::new_err("unrecognized recorder options")),
        })
    }
}

impl<G: Gene + 'static, F: Fitness + AsPrimitive<u64>>
    TryInto<RecorderBox<G, F, recorder::HdrHistogram>> for Recorder
{
    type Error = PyErr;
    fn try_into(self) -> Result<RecorderBox<G, F, recorder::HdrHistogram>, Self::Error> {
        Ok(match self {
            Recorder::HdrHistogram(options) => {
                Box::new(recorder::RecorderHdrHistogram::from(options))
            }
            _ => return Err(PyValueError::new_err("unrecognized recorder options")),
        })
    }
}
