use num_traits::{AsPrimitive, FromPrimitive};
use pyo3::prelude::*;
use ratio_genetic::alias::{Fitness, Gene, Record};
use ratio_genetic::convergence;

#[pyclass(eq, eq_int)]
#[derive(Clone, Debug, Default, PartialEq)]
pub enum ConvergenceKinds {
    #[default]
    Never,
}
#[pymethods]
impl ConvergenceKinds {
    #[new]
    pub fn new() -> Self {
        Self::default()
    }
}

#[derive(Clone, Debug, FromPyObject)]
pub enum Convergence {
    Kind(ConvergenceKinds),
}

pub type ConvergenceBox<Gene, Fitness, Record> =
    Box<dyn convergence::Convergence<Gene = Gene, Fitness = Fitness, Record = Record>>;

impl<
        G: Gene + 'static + FromPrimitive + AsPrimitive<f64>,
        F: Fitness + 'static,
        R: Record + 'static,
    > TryFrom<Convergence> for ConvergenceBox<G, F, R>
{
    type Error = PyErr;
    fn try_from(value: Convergence) -> Result<Self, Self::Error> {
        Ok(match value {
            Convergence::Kind(options) => match options {
                ConvergenceKinds::Never => Box::new(convergence::ConvergenceNever::new()),
            },
        })
    }
}
