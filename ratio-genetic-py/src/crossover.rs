use num_traits::{AsPrimitive, FromPrimitive};
use pyo3::exceptions::PyValueError;
use pyo3::prelude::*;
use rand::Rng;
use ratio_genetic::alias::{Fitness, Gene};
use ratio_genetic::crossover;

#[pyclass(eq, eq_int)]
#[derive(Clone, Debug, Default, PartialEq)]
pub enum CrossoverKinds {
    #[default]
    IPX,
    Point,
    Blend,
    SimulatedBinary,
}
#[pymethods]
impl CrossoverKinds {
    #[new]
    pub fn new() -> Self {
        Self::default()
    }
}

#[derive(Clone, Debug, FromPyObject)]
pub enum Crossover {
    Blend(CrossoverBlend),
    SimulatedBinary(CrossoverSimulatedBinary),
    Kind(CrossoverKinds),
}

#[pyclass(get_all, set_all)]
#[derive(Clone, Debug, Default)]
pub struct CrossoverBlend {
    kind: CrossoverKinds,
    pub alpha: f64,
}
#[pymethods]
impl CrossoverBlend {
    #[new]
    pub fn new(alpha: f64) -> Self {
        Self {
            kind: CrossoverKinds::Blend,
            alpha,
        }
    }
}
impl<G: Gene, F: Fitness> From<CrossoverBlend> for crossover::CrossoverBlend<G, F> {
    fn from(value: CrossoverBlend) -> Self {
        crossover::CrossoverBlend::new(value.alpha)
    }
}

#[pyclass(get_all, set_all)]
#[derive(Clone, Debug, Default)]
pub struct CrossoverSimulatedBinary {
    kind: CrossoverKinds,
    pub eta: f64,
}
#[pymethods]
impl CrossoverSimulatedBinary {
    #[new]
    pub fn new(eta: f64) -> Self {
        Self {
            kind: CrossoverKinds::SimulatedBinary,
            eta,
        }
    }
}
impl<G: Gene, F: Fitness> From<CrossoverSimulatedBinary>
    for crossover::CrossoverSimulatedBinary<G, F>
{
    fn from(value: CrossoverSimulatedBinary) -> Self {
        crossover::CrossoverSimulatedBinary::new(value.eta)
    }
}

pub type CrossoverBox<R, Gene, Fitness> =
    Box<dyn crossover::Crossover<R, Gene = Gene, Fitness = Fitness>>;

impl<
        R: Rng + ?Sized,
        G: Gene + 'static + FromPrimitive + AsPrimitive<f64>,
        F: Fitness + 'static,
    > TryFrom<Crossover> for CrossoverBox<R, G, F>
{
    type Error = PyErr;
    fn try_from(value: Crossover) -> Result<Self, Self::Error> {
        Ok(match value {
            Crossover::Blend(options) => Box::new(crossover::CrossoverBlend::from(options)),
            Crossover::SimulatedBinary(options) => {
                Box::new(crossover::CrossoverSimulatedBinary::from(options))
            }
            Crossover::Kind(options) => match options {
                CrossoverKinds::IPX => Box::new(crossover::CrossoverIPX::new()),
                CrossoverKinds::Point => Box::new(crossover::CrossoverPoint::new()),
                _ => return Err(PyValueError::new_err("unrecognized crossover options")),
            },
        })
    }
}
