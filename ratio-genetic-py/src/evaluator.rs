use std::collections::HashSet;
use std::fmt;

use num_traits::FromPrimitive;
use pyo3::exceptions::PyValueError;
use pyo3::prelude::*;
use ratio_genetic::alias::{Fitness, Gene};
use ratio_genetic::evaluator;

use crate::utils::vector_to_matrix;

#[pyclass(eq, eq_int)]
#[derive(Clone, Debug, Default, Eq, PartialEq, Hash)]
pub enum EvaluatorKinds {
    #[default]
    FeedbackDistance,
    FeedbackMarks,
    LowerLeftDistance,
    Value,
}
#[pymethods]
impl EvaluatorKinds {
    #[new]
    pub fn new() -> Self {
        Self::default()
    }
}
impl fmt::Display for EvaluatorKinds {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

#[pyclass(get_all, set_all)]
#[derive(Clone, Debug, Default)]
pub struct EvaluatorMatrix {
    pub kind: EvaluatorKinds,
    pub matrix: Vec<f64>,
    pub offset: Option<usize>,
}
#[pymethods]
impl EvaluatorMatrix {
    #[new]
    #[pyo3(signature = (kind, matrix, offset=None))]
    pub fn new(kind: EvaluatorKinds, matrix: Vec<f64>, offset: Option<usize>) -> PyResult<Self> {
        if Self::kinds().contains(&kind.to_string()) {
            Ok(Self {
                kind,
                matrix,
                offset,
            })
        } else {
            Err(PyValueError::new_err(
                "wrong evaluator kind, only those in the EvaluatorMatrix.kinds() set are valid.",
            ))
        }
    }

    #[staticmethod]
    pub fn kinds() -> HashSet<String> {
        let mut set: HashSet<String> = HashSet::new();
        set.insert(EvaluatorKinds::FeedbackDistance.to_string());
        set.insert(EvaluatorKinds::FeedbackMarks.to_string());
        set.insert(EvaluatorKinds::LowerLeftDistance.to_string());
        set
    }
}
impl<F: Fitness> TryFrom<EvaluatorMatrix> for evaluator::EvaluatorFeedbackDistance<f64, F> {
    type Error = PyErr;
    fn try_from(value: EvaluatorMatrix) -> Result<Self, Self::Error> {
        Ok(evaluator::EvaluatorFeedbackDistance::new(
            vector_to_matrix(&value.matrix)?,
            value.offset.unwrap_or(1),
        ))
    }
}
impl<F: Fitness> TryFrom<EvaluatorMatrix> for evaluator::EvaluatorFeedbackMarks<f64, F> {
    type Error = PyErr;
    fn try_from(value: EvaluatorMatrix) -> Result<Self, Self::Error> {
        Ok(evaluator::EvaluatorFeedbackMarks::new(
            vector_to_matrix(&value.matrix)?,
            value.offset.unwrap_or(1),
        ))
    }
}
impl<F: Fitness> TryFrom<EvaluatorMatrix> for evaluator::EvaluatorLowerLeftDistance<f64, F> {
    type Error = PyErr;
    fn try_from(value: EvaluatorMatrix) -> Result<Self, Self::Error> {
        Ok(evaluator::EvaluatorLowerLeftDistance::new(
            vector_to_matrix(&value.matrix)?,
            value.offset.unwrap_or(1),
        ))
    }
}

#[pyclass(get_all, set_all)]
#[derive(Clone, Debug, Default)]
pub struct EvaluatorValue {
    kind: EvaluatorKinds,
    pub value: f64,
}
#[pymethods]
impl EvaluatorValue {
    #[new]
    pub fn new(value: f64) -> Self {
        Self {
            kind: EvaluatorKinds::Value,
            value,
        }
    }
}

impl<G: Gene, F: Fitness + FromPrimitive> From<EvaluatorValue> for evaluator::EvaluatorValue<G, F> {
    fn from(value: EvaluatorValue) -> Self {
        evaluator::EvaluatorValue::new(F::from_f64(value.value).unwrap())
    }
}

#[derive(Clone, Debug, FromPyObject)]
pub enum Evaluator {
    Matrix(EvaluatorMatrix),
    Value(EvaluatorValue),
}

pub type EvaluatorBox<Gene, Fitness> =
    Box<dyn evaluator::Evaluator<Gene = Gene, Fitness = Fitness>>;

impl<F: Fitness + FromPrimitive + 'static> TryFrom<Evaluator> for EvaluatorBox<usize, F> {
    type Error = PyErr;
    fn try_from(value: Evaluator) -> Result<Self, Self::Error> {
        Ok(match value {
            Evaluator::Matrix(options) => match options.kind {
                EvaluatorKinds::FeedbackDistance => {
                    Box::new(evaluator::EvaluatorFeedbackDistance::try_from(options)?)
                }
                EvaluatorKinds::FeedbackMarks => {
                    Box::new(evaluator::EvaluatorFeedbackMarks::try_from(options)?)
                }
                EvaluatorKinds::LowerLeftDistance => {
                    Box::new(evaluator::EvaluatorLowerLeftDistance::try_from(options)?)
                }
                _ => return Err(PyValueError::new_err("unrecognized evaluator options")),
            },
            Evaluator::Value(options) => Box::new(evaluator::EvaluatorValue::from(options)),
        })
    }
}
