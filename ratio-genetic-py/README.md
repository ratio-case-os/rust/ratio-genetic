# Ratio Genetic Python Bindings

The bindings crate is currently just in a _proof of concept_ state and will for the
moment just follow the main crate's versioning.

This package provides a thin wrapper around the Ratio Genetic crate written in Rust. It
is built with Maturin utilizing PyO3 bindings for convenience.

To get up and running quickly, a Poetry section has been added to the `pyproject.toml`.
This installs a Poetry environment with Maturin and PyTest. After any changes to the
Rust code you should probably run `poetry run maturin develop` and `poetry run pytest`
using your preferred method.
